'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    path.join(conf.paths.src, '/app/**/*.html'),
    path.join(conf.paths.src, '/app/components/**/*.html'),
    path.join(conf.paths.tmp, '/serve/app/**/*.html')
  ])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'secual',
      root: 'app'
    }))
    .pipe(gulp.dest(conf.paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(path.join(conf.paths.tmp, '/partials/templateCacheHtml.js'), { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: path.join(conf.paths.tmp, '/partials'),
    addRootSlash: false
  };

  var htmlFilter = $.filter('*.html', { restore: true });
  //var jsFilter = $.filter(['**/*.js', '!**/*.js.map', '!**/*.ts'], { restore: true });
  var jsFilter = $.filter('**/*.js', { restore: true });
  var cssFilter = $.filter('**/*.css', { restore: true });
  var assets;

  return gulp.src(path.join(conf.paths.tmp, '/serve/*.html'))
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    .pipe($.rev())
    .pipe(jsFilter)
    .pipe($.sourcemaps.init())
    .pipe($.ngAnnotate())
    .pipe($.uglify({ preserveComments: $.uglifySaveLicense, mangle:false })).on('error', conf.errorHandler('Uglify'))
    .pipe($.sourcemaps.write('maps'))
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe($.sourcemaps.init())
    .pipe($.replace('../../bower_components/material-design-iconfont/iconfont/', '../fonts/'))
    .pipe($.minifyCss({ processImport: false }))
    .pipe($.sourcemaps.write('maps'))
    .pipe(cssFilter.restore)
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true,
      conditionals: true
    }))
    .pipe(htmlFilter.restore)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')))
    .pipe($.size({ title: path.join(conf.paths.dist, '/'), showFiles: true }));
  });

// Only applies for fonts from bower dependencies
// Custom fonts are handled by the "other" task
gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles().concat('bower_components/material-design-iconfont/iconfont/*'))
    .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/fonts/')));
});

gulp.task('vendors', function () {
  var fileFilter = $.filter(function (file) {
    return file.stat.isFile();
  });

  return gulp.src([
    path.join(conf.paths.vendors, '/*')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/'+conf.paths.vendors)));
});

gulp.task('built_styles', function () {
 var copiedFiles = gulp.src([
    path.join(conf.paths.src, '/app/common/*.scss'),
    path.join(conf.paths.src, '/app/components/**/*.scss')
  ], { read:true });

  return copiedFiles
    //.pipe($.sourcemaps.init())
    .pipe($.sass()).on('error', conf.errorHandler('Sass'))
    .pipe($.autoprefixer()).on('error', conf.errorHandler('Autoprefixer'))
    //.pipe($.sourcemaps.write())
    //.pipe(minifycss())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/assets/styles/css/')));
});

gulp.task('static_styles', function () {
 var copiedFiles = gulp.src([
    path.join(conf.paths.src, '/assets/styles/**/*.scss')
  ], { read:true, base: path.join(conf.paths.src, '/assets/styles/') });

  return copiedFiles
    .pipe($.sourcemaps.init())
    .pipe($.sass()).on('error', conf.errorHandler('Sass'))
    .pipe($.autoprefixer()).on('error', conf.errorHandler('Autoprefixer'))
    .pipe($.sourcemaps.write())
    //.pipe(minifycss())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/assets/styles/css/')));
});

gulp.task('static_pages', function () {
 var copiedFiles = gulp.src([
    path.join(conf.paths.src, '/static/*.html')
  ], { read:true, base: path.join(conf.paths.src, '/static/') });

  return copiedFiles
    .pipe($.sourcemaps.init())
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/static/')));
});

gulp.task('i18nLocaleFiles', function () {
  var allFiles =[];
  allFiles.push('bower_components/angular-i18n/angular-locale_en-us.js');
  allFiles.push('bower_components/angular-i18n/angular-locale_ja-jp.js');

  return gulp.src(allFiles)
    .pipe($.flatten())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/i18n/')));
});

gulp.task('copy_resources', function () {
  return gulp.src([path.join(conf.paths.src, '/resources/*')])
    .pipe($.flatten())
    .pipe(gulp.dest(path.join(conf.paths.dist, '/resources/')));
});

gulp.task('other', function () {
  var fileFilter = $.filter(function (file) {
    return file.stat.isFile();
  });

  return gulp.src([
    path.join(conf.paths.src, '/**/*'),
    path.join('!' + conf.paths.src, '/**/*.{html,css,js,scss}'),
    path.join(conf.paths.vendors, '/*')
  ])
    .pipe(fileFilter)
    .pipe(gulp.dest(path.join(conf.paths.dist, '/')));
});

gulp.task('clean', function () {
  return $.del([path.join(conf.paths.dist, '/'), path.join(conf.paths.tmp, '/')]);
});

gulp.task('build', ['html', 'fonts', 'other', 'static_pages', 'built_styles', 'static_styles', 'vendors', 'i18nLocaleFiles', 'copy_resources']);
