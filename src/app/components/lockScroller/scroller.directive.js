(function() {
  'use strict';

  angular
    .module('secual')
    .directive('secualScroller', secualScroller)

  /** @ngInject */
  secualScroller.$inject =  ['UserService'];
  function secualScroller() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/lockScroller/scroller.html',
      scope: {
          backButton: '=',
          closeButton: '=',
          title: '='
      },
      link: link,
      replace:true,
      controller: ScrollerController,
      controllerAs: 'scroller',
      bindToController: true
    };

    return directive;

    function link(scope, element, attrs, controller){
      var progressChecker;
      var loadingClass = 'loading';
      var lockToggleThreshold = 50;
      var accountId = 0;

      scope.$parent.base.menuButton = true;

      var setup = function(){

            var pages = $(".lockpattern");
                pages.width($(window).width());
                $("#scroller").width(pages.length * 100 + "%");
                var pageIndicator = $("#pageIndicator");
                for (var i=0; i<pages.length; i++) {
                  var el = i==0 ? "<li class='active'></li>" : "<li></li>";
                  pageIndicator.append(el);
                }

          identifyLockStatus().then(function(response){

              var user = response.account;
              accountId = user.accountID;
              var lockStatus = user.lock;

              if (lockStatus){
                var scr = $('#scroller');
                var lockp = scr.find('.lockpattern').first();
                lockp.toggleClass("locked");
                lockp.find(".lock-status").toggleClass("active");
                lockp.find("p.status").text(lockp.hasClass("locked") ? "長押しでアンロック" : "長押しでロック");
              }
          })
      }

      function identifyLockStatus(){
         return controller.getLockStatus();
      }

      setup();

       var lockPattern = new IScroll('#main', {
        scrollX: true,
        scrollY: false,
        mouseWheel: false,
        snap: true,
        eventPassthrough: true
      });

      var $activePattern;
      var $progress;
      var lockCycleComplete = true
      var perimeter = 240 * 3.14592;

      var checkLockProgress = function(){
        var progress = $progress.css("stroke-dashoffset").replace("px", "");
        if (progress < lockToggleThreshold && !lockCycleComplete){
        lockCycleComplete = true;
        var lockp = $progress.closest(".lockpattern");
        lockp.toggleClass("locked");
        lockp.find(".lock-status").toggleClass("active");
        lockp.find("p.status").text(lockp.hasClass("locked") ? "長押しでアンロック" : "長押しでロック");
        $progress.parent().attr('class', $progress.parent().attr('class').split(' ')[0]);
        $progress.parent().children(".animate").hide();

        var lockStatus = lockp.hasClass("locked") ? true : false;
        lockAccount(accountId,lockStatus);
        setTimeout(function(){
          $progress.show()}, 300);
        }
      }

       function lockAccount(accountId,lockStatus){
        controller.lockAccount(accountId,lockStatus);
       }


  lockPattern.on('scrollEnd', function(){
    var page = this.currentPage.pageX;
    $("#pageIndicator li").removeClass("active").eq(page).addClass("active");
  })
  lockPattern.on("scrollStart", function(){
    if ($activePattern) {
      $activePattern.trigger("mouseup");
    }
  })

  $(".lock")
    .on("mousedown touchstart", function(e){
      console.log("touchdown");
       e.preventDefault();
      $activePattern = $(this);
      var item = $(this).children(".lock-progress");
      $progress = item.find("circle.animate");
      lockCycleComplete = false;
      if (!item.hasClass(loadingClass)){
        item.attr('class', item.attr('class') + " "+ loadingClass);
        //item.addClass(loadingClass);
        progressChecker = setInterval(checkLockProgress, 10);
      }
    })
    .on("mouseup touchend", function(e){
      lockCycleComplete = true;
      $(this).children(".lock-progress").attr('class', $progress.parent().attr('class').split(' ')[0]);
      clearInterval(progressChecker);
      //lockAccount();
    })

    }

    /** @ngInject */
    function ScrollerController($timeout, $mdSidenav, $log, UserService, $filter) {
      var vm = this;
       vm.default = $filter('translate')("default");
    vm.lock_icons_long_press = $filter('translate')("lock_icons_long_press");
    vm.lock_window_long_press = $filter('translate')("lock_window_long_press");
    vm.office_safe = $filter('translate')("office_safe");
    vm.shopping = $filter('translate')("shopping");

      vm.toggleRight = buildToggler('left');
      vm.lockAccount = lockAccount;
      vm.getLockStatus = getLockStatus;

      function getLockStatus(){
          return UserService.get();
      }


     function lockAccount(accountID, lockStatus){
        UserService.lock(accountID, lockStatus).then(function(response){

        }, function(failed){
          alert(failed.statusText);
        })
      }

      function buildToggler(navID) {
        return function() {
          $mdSidenav(navID)
            .toggle()
            .then(function () {
              //$log.debug("toggle " + navID + " is done");
            });
        }
      }

    }
  }

})();
