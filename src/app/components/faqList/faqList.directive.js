(function() {
  'use strict';

  angular
    .module('secual')
    .directive('secualFaqList', SecualFaqList)

  /** @ngInject */
  function SecualFaqList() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/faqList/faqList.directive.html',
      scope: {
         faqs: '='
      }
    };

    return directive;
  }

  
})();



