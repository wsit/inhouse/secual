(function() {
  'use strict';

  angular
   .module('secual')
   .directive('secualInput', secualInputDirective)

  /** @ngInject */
  function secualInputDirective() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/secualInput/secualInput.html',
      scope: {
          value: '=',
          placeholder: '='
      },
      link: Inputlink,
      replace:true
      //controller: secualInputController,
      //controllerAs: 'secualInput',
      //bindToController: true
    };

    return directive;
  }

  function Inputlink(scope, element, attrs){
      var input = element.find('input');
      var icon = element.find('i');
      input.on('focus', function () {
        icon.show();
      })

      input.on('blur', function () {
        icon.hide();
      })

      icon.on('mousedown', function(event) {
        event.preventDefault();
      }).on('click', function(){
        input.val('');
        //input.focus();
      })

  }

})();
