(function() {
  'use strict';

  angular
    .module('secual')
    .directive('secualTabs', secualTabs)
    .directive('secualTabPane', secualTabPane);

  function secualTabs() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/tabs/tabs.html',
      transclude: true,
      scope: {},
      /** @ngInject */
      controller: function($scope) {
        var panes = $scope.panes = [];

        $scope.select = function(pane) {
          angular.forEach(panes, function(pane) {
            pane.selected = false;
          });
          pane.selected = true;
        };

        this.addPane = function(pane) {
          if (panes.length === 0) {
            $scope.select(pane);
          }
          panes.push(pane);
        };
      }
    };
  }

  function secualTabPane() {
    return {
      restrict: 'E',
      templateUrl: 'app/components/tabs/tabPane.html',
      require: '^^secualTabs',
      transclude: true,
      scope: {
        title: '@'
      },
      link: function(scope, elem, attrs, tabsCtrl) {
        tabsCtrl.addPane(scope);
      }
    };
  }

})();
