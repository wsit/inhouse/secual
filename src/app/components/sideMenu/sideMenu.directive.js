(function() {
  'use strict';

  angular
  .module('secual')
  .directive('secualSideMenu', secualSideMenu);

  /** @ngInject */
  function secualSideMenu() {
    var directive = {
      restrict: 'E',
      scope: {
        info: '='
      },
      templateUrl: 'app/components/sideMenu/sideMenu.html',
      controller: SideMenuController,
      controllerAs: 'sidemenu',
      bindToController: true
    };

    return directive;
  }

    /** @ngInject */
    function SideMenuController($mdSidenav, $log, $state, utils, $filter) {
      var vm = this;
      vm.close = function () {
        $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });
      };

      vm.goTo = function (newState, params){
         vm.close();
        $state.go('app.'+newState, params);

      };

      vm.goToShop = utils.goToShop
      vm.HOME = $filter('translate')("HOME");
      vm.security_settings = $filter('translate')("security_settings");
      vm.GROUP = $filter('translate')("GROUP");
      vm.USERS = $filter('translate')("USERS");
      vm.ACCOUNT = $filter('translate')("ACCOUNT");
      vm.SUPPORT = $filter('translate')("SUPPORT");;
    }

})();
