(function() {
  'use strict';

  angular
   .module('secual')
   .directive('secualEditableLabel', secualEditableLabelDirective)

  /** @ngInject */
  function secualEditableLabelDirective() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/secualEditableLabel/secualEditableLabel.html',
      scope: {
          value: '=',
          action: '='
      },
      link: Labellink,
      replace:true,
      controller: secualEditableLabelController,
      controllerAs: 'secualEditableLabel'
      //bindToController: true
    };

    return directive;
  }

  function Labellink(scope, element, attrs, controller){
      var input = element.find('input');
      var span = element.find('span');
      var editIcon = element.find('#iconEditableLabelEdit');
      var saveIcon = element.find('#iconEditableLabelSave');

      //input.on('focus', function () {
      //  icon.show();
      //})

      input.on('blur', function () {
         input.hide();
        span.show();
        saveIcon.hide();
        editIcon.show();
      })

      editIcon.on('mousedown', function(event) {
        event.preventDefault();
      }).on('click', function(){
        //input.val('');
        span.hide();
        input.show();
        editIcon.hide();
        saveIcon.show();
        input.focus();
      });

      saveIcon.on('mousedown', function(event) {
        event.preventDefault();
      }).on('click', function(){
        //input.val('');
        if (angular.isFunction(scope.action)){
          scope.action().then(function(response){
             input.hide();
             span.show();
             saveIcon.hide();
             editIcon.show();
             //controller.showMessage('success', 'SAVED');
          }, function (response) {
             //controller.showMessage('error', 'something_went_wrong');
          });
        }
        else {
          input.hide();
             span.show();
             saveIcon.hide();
             editIcon.show();
        }
        
      });

  }

  function secualEditableLabelController($filter, utils){
    var vm = this;
    vm.showMessage = showMessage;
    //vm.action =  

    function showMessage(messageType,key){
      utils.showMessage(messageType, $filter('translate')(key));
    }

  }

})();
