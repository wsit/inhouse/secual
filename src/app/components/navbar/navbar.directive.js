(function() {
  'use strict';

  angular
    .module('secual')
    .directive('secualNavbar', secualNavbar)
    .directive('secualPrevButton', secualPrevButton);

  function secualPrevButton() {
    return {
      restrict: 'A',
      controller: BbController,
      controllerAs: 'bb',
      bindToController: true
    };

    function BbController($window, $scope, $timeout, $filter) {
     var vm = this;
     vm.back = $filter('translate')("back");
     vm.save = $filter('translate')("save");
     vm.connection = $filter('translate')("connection");

    }
  }


  /** @ngInject */
  function secualNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          backButton: '=',
          closeButton: '=',
          closeButtonRight: '=',
          editButton: '=',
          checkButton: '=',
          saveButton: '=',
          menuButton: '=',
          addButton: "=",
          cancelButton: "=",
          notificationButton: "=",
          sendMailButton: "=",
          connectButton: "=",
          title: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($window, $mdSidenav) {
      var vm = this;

      vm.handleButtonClick = function(button) {
         $('#content-container').addClass("view_transition_backward");
        if (angular.isFunction(button)) {
          button();
        }
      };

      vm.handleBackButton = function() {
        $('#content-container').removeClass("view_transition_forward");
        $('#content-container').addClass("view_transition_backward");
        if (angular.isFunction(vm.backButton)) {
          vm.backButton();
        } else if (angular.isFunction(vm.backButton.action)) {
          vm.backButton.action();
        } else {
          $window.history.back();
        }
      };

      vm.toggleRight = buildToggler('left');
      vm.isOpenRight = function() {
        return $mdSidenav('left').isOpen();
      };

      function buildToggler(navID) {
        return function() {
          $mdSidenav(navID).toggle();
        }
      }
    }
  }

})();
