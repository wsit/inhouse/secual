(function() {
  'use strict';

  angular
    .module('secual')
    .directive('secualBackButton', secualBackButton);

	function secualBackButton($window) {
     return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.bind('click', function () {
                    $window.history.back();
                });
            }
        }
	}

});
