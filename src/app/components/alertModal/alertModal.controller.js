(function() {
  'use strict';

  angular
    .module('secual')
    .controller('ModalController', ModalController);

  /** @ngInject */
  function ModalController($rootScope, $scope, close, $filter) {
    var vm = this;

    $scope.confirmation_required = $filter('translate')("confirmation_required");
    $scope.are_you_sure_you_want_to_proceed = $filter('translate')("are_you_sure_you_want_to_proceed");
    $scope.NO = $filter('translate')("NO");
    $scope.YES = $filter('translate')("YES");

      $scope.closeModal = function(caption){
            close(caption);
        }
  
  }
})();
