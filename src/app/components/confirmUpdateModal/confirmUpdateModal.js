(function() {
  'use strict';

  angular
    .module('secual')
    .controller('ConfirmUpdateModalController', ConfirmUpdateModalController);

  /** @ngInject */
  function ConfirmUpdateModalController($rootScope, $scope, close, $filter) {

     $scope.CONFIRM_UPDATE = $filter('translate')("CONFIRM_UPDATE");
     $scope.NEW_VERSION_IS_AVAILABLE = $filter('translate')("NEW_VERSION_IS_AVAILABLE");
     $scope.cancel = $filter('translate')("cancel");
     $scope.update = $filter('translate')("update");

      $scope.closeModal = function(caption){
            close(caption);
        }
  
  }
})();
