(function() {
  'use strict';

  angular
    .module('secual')
    .controller('UserEditController', UserEditController);

  /** @ngInject */
  function UserEditController(toastr, user, UserService, $scope, $state, ModalService, $filter, utils) {
    var vm = this;
    vm.user = user;
    vm.newUser = angular.isUndefined(user.id);
    vm.errors = {};
    vm.update = updateUser;
    vm.delete = deleteUser;
    vm.validators = {
      name: validateName,
      password: validatePassword,
      confirmPassword: validateConfirmPassword,
      email: validateEmail
    };

    vm.validatorsNewUser = {
      name: validateName,
      email: validateEmail
    };

    vm.add_pictures = $filter('translate')("add_pictures");
    vm.name = $filter('translate')("name");
    vm.PASSWORD = $filter('translate')("password");
    vm.confirm_password = $filter('translate')("confirm_password");
    vm.EMAIL = $filter('translate')("EMAIL");
    vm.phone = $filter('translate')("phone");
    vm.delete_user = $filter('translate')("delete_user");

    $scope.$parent.base.title = $filter('translate')("User")+ (vm.newUser ? $filter('translate')("NEW") : $filter('translate')("EDIT"));

    $scope.$parent.base.setButtons({
      cancelButton: function() {
        if (!vm.newUser)
          $state.go('app.users.details', {id: vm.user.id});
        else
          $state.go('app.users.list');
      },
      checkButton: vm.newUser ? createUser : updateUser
    });

    function alertDeleteUser() {
      ModalService.showModal({
        templateUrl: "app/components/alertModal/alertModal.html",
        controller: "ModalController"
      }).then(function(modal) {
        //it's a bootstrap element, use 'modal' to show it
        modal.element.modal();
        modal.close.then(function(result) {
          $(".modal-backdrop").css("display", "none");
          console.log("result", result);
          if (result == 'Yes') {
            UserService.delete(vm.user.id).then(function(result) {
              toastr.success($filter('translate')("REMOVED"));
              $state.go('app.users.list');
            }, function(response) {
              if (vm.user && vm.user.role.name == 'owner')
                utils.showMessage('error', $filter('translate')("unable_to_delete_the_administrator"));
              else
                utils.showMessage('error', $filter('translate')("saving_changes_failed"));
            });
          }
        });
      })
    }

    function validateName() {
      vm.errors.name = '';
      if (!vm.user.name || !vm.user.name.trim()) {
        vm.errors.name = $filter('translate')("user_name_cannot_be_empty");
        return false;
      }
      return true;
    }

    function validatePassword() {
      vm.errors.password = '';
      if (!vm.newUser && !vm.user.password) {
        validateConfirmPassword();
        return true;
      }
      var passwReg = /[^A-Za-z0-9 ]/;
      var passedTest = !vm.user.password.match(/\d/) || !vm.user.password.match(/[a-z]/i);
      if (vm.user.password.length < 8 || passedTest) {
        vm.errors.password = $filter('translate')("password_must_contain_at_least_8_of_alpha_and_numeric_characters");
        return false;
      }
      return true;
    }

    function validateConfirmPassword() {
      vm.errors.confirmPassword = '';
      if (vm.user.confirmPassword !== vm.user.password) {
        vm.errors.confirmPassword = $filter('translate')("PASSWORD_AND_CONFIRMATION_PASSWORD_SHOULD_BE_EQUAL");
        return false;
      }
      return true;
    }


    function validateEmail() {
      vm.errors.email = '';
      vm.user.email = (vm.user.email || '').trim();
      if (!vm.user.email) {
        vm.errors.email = $filter('translate')("email_cannot_be_empty");
        return false;
      }
      if (!/\S+@\S+\.\S+/.test(vm.user.email)) {
        vm.errors.email = $filter('translate')("INVALID_EMAIL");
        return false;
      }
      return true;
    }


    function validateForm() {
      if(vm.newUser){
        return Object.keys(vm.validatorsNewUser).map(function(name) {
          return vm.validators[name]();
        }).every(function(valid) {
          return valid;
        });
      }
      else{
        return Object.keys(vm.validators).map(function(name) {
          return vm.validators[name]();
        }).every(function(valid) {
          return valid;
        });
      }
    }

    function createUser() {
      if (!validateForm()) {
        return;
      }

      //console.log(vm.user);
      var userObj = {"user": vm.user, "confirm_success_url": "http://development.getsecual.com/"};
      userObj.user.alert_recipient= true,

        UserService.create(userObj).then(function(result) {
          $state.go('app.users.list');
          toastr.success($filter('translate')("user_created"));
        }, function(response) {
          if (response.data){
            var errors = response.data.errors;
            var result = "";
            for (var i in errors) {
              if (errors.hasOwnProperty(i)) {
                //var prop = errors.keys(i);
                result += errors[i];
                toastr.error(result);
              }
            }
          }
          else {
            utils.showMessage('error', $filter('translate')("error_during_update_occures_please_try_again_later"));
          }
        });
    }

    function updateUser() {
          $('#content-container').removeClass("view_transition_backward");
          $('#content-container').addClass("view_transition_forward");
            if (!validateForm()) {
                return;
            }

      if (vm.password) {
        vm.user.password = vm.password;
        vm.user.confirm_password = vm.confirmPassword;
      }

      if ($scope.$parent.base.user.role.name == 'owner' && vm.user.role.name == 'owner')
        UserService.update(vm.user).then(function(result) {
          toastr.success($filter('translate')("UPDATED"));
          $scope.$parent.base.user.name = vm.user.name;
          $state.go('app.users.details', { id: user.id });
        }, function(response) {
          toastr.error($filter('translate')("saving_changes_failed"));
          var error = response.data.error || {};
          if (error.email) {
            vm.errors.email = $filter('translate')("EMAIL")  + error.email[0];
          }
        });
      else {
        UserService.updateByAdmin(vm.user, vm.user.id).then(function(result) {
          toastr.success($filter('translate')("UPDATED") );
          $state.go('app.users.details', { id: user.id });
        }, function(response) {
          toastr.error($filter('translate')("saving_changes_failed"));
          var error = response.data.error || {};
          if (error.email) {
            vm.errors.email = $filter('translate')("EMAIL")  + error.email[0];
          }
        });
      }
    }

    function deleteUser() {
      alertDeleteUser();

    }
  }
})();
