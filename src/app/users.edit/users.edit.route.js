(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.users.add', {
        url: '/new',
        templateUrl: 'app/users.edit/users.edit.html',
        controller: 'UserEditController',
        controllerAs: 'userEditInfo',
        resolve: {
          user: function() {
            return {};
          }
        }
      })
      .state('app.users.edit', {
        url: '/edit/:id',
        templateUrl: 'app/users.edit/users.edit.html',
        controller: 'UserEditController',
        controllerAs: 'userEditInfo',
        resolve: {
          user: function(UserService, $stateParams) {
            return UserService.getById($stateParams.id);
          }
        }
      });
  }

})();
