(function() {
  'use strict';

  angular
    .module('secual')
    .controller('GatewaysController', GatewaysController);

  /** @ngInject */
  function GatewaysController(toastr, gateways, patterns, UserGatewayService, $scope, $state, utils, UserService, $filter) {
    var vm = this;
    vm.user = $scope.$parent.base.user;
    vm.gateways = gateways.plain().gateways;
    vm.patterns = patterns.plain().patterns;
    vm.getData = getdata;
    vm.goToPattern = goToPattern;
    vm.goToDetails = goToDetails;
    vm.goToShop = utils.goToShop;

    vm.goTo = utils.goTo;

    vm.gateway = $filter('translate')("GATEWAY");
    vm.SENSORS = $filter('translate')("SENSORS");
    vm.SENSOR = $filter('translate')("SENSOR");
    vm.ADD_MORE_DEVICE = $filter('translate')("ADD_MORE_DEVICE");
    vm.BUY_ADDITIONAL_SENSOR = $filter('translate')("BUY_ADDITIONAL_SENSOR");
    vm.LOCK_PATTERN = $filter('translate')("LOCK_PATTERN");
    vm.ADD_LOCK_PATTERN = $filter('translate')("ADD_LOCK_PATTERN");

    // $scope.$parent.base.title = 'Security Settings';
    $scope.$parent.base.title = 'セキュリティ設定';
    $scope.$parent.base.setButtons({
      menuButton: true
    });

    function getdata(id){
      UserGatewayService.getById(id).then(function(result){
        toastr.success("Saved");
      }, function(response){
        toastr.error(response.message);
      })
    }

    function goToPattern(patternId){
      if(patternId) {
        $state.go('app.gateways.pattern', {id:patternId});
      }
      else{
         //if (vm.currentPatternId)
       $state.go('app.gateways.new-pattern');
      //else utils.showMessage('error', 'Current pattern id is not set');

      }
    }

    function goToDetails(gatewayId){
      if(gatewayId) {
        $state.go('app.gateways.details', {id:gatewayId});
      }
      else{
        $state.go('app.gateways.details');
      }
    }

  }
})();
