(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.gateways', {
        url: '/gateways',
        abstract: true,
        template: '<ui-view />'
      })

      .state('app.gateways.list', {
        url: '',
        templateUrl: 'app/gateways/gateways.html',
        controller: 'GatewaysController',
        controllerAs: 'gatewaysList',
        resolve: {
          gateways: function(UserGatewayService){
            return UserGatewayService.get();
          },
          patterns: function (UserGatewayService) {
            return UserGatewayService.getPatterns();
          }
        },
        data:{
            roles: ['owner', 'user'],
            isSecuritySettable: true
        }
      });

  }

})();
