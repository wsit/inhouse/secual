(function() {
  'use strict';

  describe('controllers', function(){
    var vm;
    var $timeout;
    var toastr;

    beforeEach(module('sources'));
    beforeEach(inject(function(_$controller_, _$timeout_, _webDevTec_, _toastr_) {
      spyOn(_webDevTec_, 'getTec').and.returnValue([{}, {}, {}, {}, {}]);
      spyOn(_toastr_, 'info').and.callThrough();

      vm = _$controller_('MainController');
      $timeout = _$timeout_;
      toastr = _toastr_;
    }));

    it($('translate')("should_have_a_timestamp_creation_date"), function(){
      expect(vm.creationDate).toEqual(jasmine.any(Number));
    });

    it($('translate')("should_define_animate_class_after_delaying_timeout"), function() {
      $timeout.flush();
      expect(vm.classAnimation).toEqual('rubberBand');
    });

    it($('translate')("should_show_a_toastr_info_and_stop_animation_when_invoke_showToastr"), function() {
      vm.showToastr();
      expect(toastr.info).toHaveBeenCalled();
      expect(vm.classAnimation).toEqual('');
    });

    it($('translate')("should_define_more_than_five_awesome_things"), function() {
      expect(angular.isArray(vm.awesomeThings)).toBeTruthy();
      expect(vm.awesomeThings.length === 5).toBeTruthy();
    });
  });
})();
