(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        data:{
          protected: true
        }//,
        /*resolve: {
          auth: function($auth, NativeAuthFactory) {
            return $auth.validateUser();
          }//,
          //user: function(UserService){
          //    //return false;
          //    return UserService.get();
          //}
        }*/
      });

  }

})();
