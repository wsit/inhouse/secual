(function() {
  'use strict';

  angular
    .module('secual')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(toastr, $scope) {
    var vm = this;

    

    $scope.$parent.base.title = '';
    $scope.$parent.base.setButtons({
      menuButton: true,
      notificationButton: true
    });


    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

  }
})();
