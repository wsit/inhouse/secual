(function() {
    'use strict';

    angular
        .module('secual')
        .service('MainService', MainService);

    MainService.$inject = ['Restangular', '$q'];
    function MainService(Restangular, $q) {
        var userService = {
                update: update,
                get: get
            };
        
        

        var userRepository = Restangular.one('user');

        function get() {             
            return userRepository.one('accounts').get();
        }

        function update(accountId, locked) {
           var updObject = {
                            "account": {
                                        "locked": locked
                                       }
                           };
           return userRepository.one('accounts', accountId).one('lock').customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})    
        }

        return userService;
    }
})();
