(function() {
    'use strict';

    angular
        .module('secual')
        .service('UserService', UserService);

    UserService.$inject = ['Restangular'];
    function UserService(Restangular) {
        var userService = {
                update: update,
                updateByAdmin: updateByAdmin,
                lock: lock,
                get: get,
                getUserAccounts: getUserAccounts,
                getDetails: getDetails,
                getUsers: getUsers,
                getById: getById,
                delete: remove,
                create: create,
                getSubscriptionPlans: getSubscriptionPlans,
                getSubscription: getSubscription,
                updateSubscription: updateSubscription,
                addSubscription: addSubscription,
                cancelSubscription: cancelSubscription,
                getCreditCards: getCreditCards,
                addCreditCard: addCreditCard,
                updateCreditCard: updateCreditCard,
                removeCreditCard: removeCreditCard
            };

        var usersRepository = Restangular.one('users');
        var userRepository = Restangular.one('user');
        var subscriptionRepository = userRepository.one('subscription');
        var paymentRepository = userRepository.one('payment');

        var authRepository = Restangular.one('auth'); 

        function get() {
            return usersRepository.one('accounts').get();
        }

        function getUserAccounts() {
            return userRepository.one('accounts').get();
        }

        function getDetails(){
          return authRepository.one('validate_token').get();
        }

        function getUsers() {
          return usersRepository.one('users').get();
        }

        function getSubscription(){
           return subscriptionRepository.get();
        }

         function getSubscriptionPlans(){
           return subscriptionRepository.one('plans').get();
        }

        function addSubscription(planId){
           return subscriptionRepository.customPOST({"plan_id": planId}, null, undefined, {'Content-Type': 'application/json'});
        }

        function updateSubscription(planId){
           return subscriptionRepository.customPUT({"plan_id": planId}, null, undefined, {'Content-Type': 'application/json'});
        }

        function cancelSubscription(){
            return subscriptionRepository.remove();
        }

        function getCreditCards(){
            return paymentRepository.get();
        }

        function addCreditCard(card_no, expire){
            return paymentRepository.customPOST({"card_no": card_no, "expire": expire}, null, undefined, {'Content-Type': 'application/json'});
        }

        function updateCreditCard(card_no, expire, card_seq){
            return paymentRepository.customPUT({"card_seq": card_seq, "card_no": card_no, "expire": expire}, null, undefined, {'Content-Type': 'application/json'});
        }

        function removeCreditCard(card_seq){
            return paymentRepository.customREMOVE({"card_seq": card_seq}, null, undefined, {'Content-Type': 'application/json'});
        }

        /*
        function update(user) {
          var updObject = { "user": user };
          return userRepository.one('users', user.id).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})
        }
        */

        function update (user) {
          return authRepository.customPUT(user, null, undefined, {'Content-Type': 'application/json'});         
        }

        function updateByAdmin (user, userid) {
          return userRepository.one('users', userid).customPUT(user, null, undefined, {'Content-Type': 'application/json'});         
        }

         function lock(accountId, locked) {

          var updObject = {
                            "account": {
                                        "lock": locked
                                       }
                           };
           return usersRepository.one('accounts', accountId).one('lock').customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})

        }

        function getById(accountId) {

          /*return { authMethod: 1,
                     created_at: "2016-01-16T09:53:52.059Z",
                     email: "user3@secual.com",
                     group_id: 1,
                     id: 5,
                     image: null,
                     name: "user3",
                     nickname: "user3",
                     phone: "*",
                     provider: "email",
                     role: {},
                     role_id: 3,
                     uid: "user3@secual.com",
                     updated_at: "2016-01-17T05:37:33.204Z"
                    };
                   */
            return usersRepository.one('users', accountId).get();
        }

        function create(data){
          var userObject = { "user": data };

          return usersRepository.one('users').customPOST(userObject, '', {}, {'Content-Type': 'application/json'});
        }

        function remove(userId){

            return usersRepository.one('users', userId).remove();

            //var deferred = $q.defer();
            //deferred.resolve({result: 'ok'});
            //return deferred.promise;
        }

        return userService;
    }
})();
