(function() {
    'use strict';

    angular
        .module('secual')
        .service('OperatorUserService', OperatorUserService);

    OperatorUserService.$inject = ['Restangular'];
    function OperatorUserService(Restangular) {
        var operatorUserService = {
                update: update,
                get: get,
                getById: getById
            };
        
        

        var operatorRepository = Restangular.one('operator');

        function get() {             
            return operatorRepository.one('accounts').get();
        }

        function update(accountId, updObject) {
          
           return operatorRepository.one('accounts', accountId).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})    
        }

        function getById(accountId) {
           return operatorRepository.one('accounts', accountId).get();
        }

        return operatorUserService;
    }
})();
