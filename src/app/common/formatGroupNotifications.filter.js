(function() {
  'use strict';

  angular
    .module('secual')
    .filter('formatGroupNotifications', FormatGroupNotificationsFilter);

  function FormatGroupNotificationsFilter($filter) {
    return function(group) {
      if (!group) {
        return '';
      }

      var notifications = [];
      if (group.push) {
        notifications.push($filter('translate')("push"));
      }
      if (group.call) {
        notifications.push($filter('translate')("call"));
      }
      if (group.sms) {
        notifications.push($filter('translate')("sms"));
      }
      if (group.email) {
        notifications.push($filter('translate')("EMAIL"));
      }
      if (group.lockable) {
        notifications.push($filter('translate')("lockable"));
      }
      if (group.admin) {
        notifications.push($filter('translate')("Administratable"));
      }
      if (group.alertstoppable) {
        notifications.push($filter('translate')("alert_stoppable"));
      }
      return notifications.join(', ');
    }
  }
})();
