(function() {
  'use strict';

  angular
    .module('secual')
    .filter('sortUsers', SortUsersFilter);

  function SortUsersFilter() {
    return function(users) {
      return (users || []).sort(function(a, b) {
        if (a.role && a.role.name === 'owner') {
          return -1;
        }
        return a.id - b.id;
      });
    }
  }
})();
