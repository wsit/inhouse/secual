(function() {
    'use strict';

    angular
        .module('secual')
        .service('UserGatewayService', UserGatewayService);

    UserGatewayService.$inject = ['Restangular'];
    function UserGatewayService(Restangular) {
        var gatewayService = {
              getSecurities: getSecurities,
              update: update,
              get: get,
              save: save,
              getById: getById,
              getSensors: getSensors,
              getSensorById: getSensorById,
              updateSensorData: updateSensorData,
              getPatterns: getPatterns,
              getPatternById: getPatternById,
              savePatternName: savePatternName,
              createPattern: createPattern,
              deletePattern: deletePattern,
              updateSensorLock: updateSensorLock,
              confirmGatewayUpdate: confirmGatewayUpdate
          };


        var userRepository = Restangular.one('user');

        function confirmGatewayUpdate(gatewayId) {
          return userRepository.one('gateways', gatewayId).one('confirm_update').customPUT({mcu: true, wifi:true, af:true} , null, undefined, {'Content-Type': 'application/json'});
        }

        function getSecurities() {
          return userRepository.one('securities').get();
        }

        function get() {
            return userRepository.one('gateways').get();
        }

        function save(gateway) {
           var updObject = {"gateway": gateway};
           return userRepository.one('gateways', gateway.id).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'});
        }


        function update(gateway) {
           var updObject = {"gateway": gateway};
           return userRepository.one('gateways', gateway.gatewayid).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'});
        }

        function getById(gatewayId) {
           return userRepository.one('gateways', gatewayId).get();
        }

        function getSensors(gatewayId){
          return userRepository.one('gateways', gatewayId).one('sensors').get();
        }

        function getSensorById(gatewayId, sensorId) {
          return userRepository.one('gateways', gatewayId).one('sensors', sensorId).get();
        }

        function updateSensorData(gatewayId, sensor) {
          var updObject = {"sensor": sensor};
          return userRepository.one('gateways', gatewayId).one('sensors', sensor.id).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'});
        }

        function getPatterns() {
          return userRepository.one('patterns').get();
        }

        function createPattern(patternName, accountID, patternLocks) {
          var pattern = {"name": patternName, "account_id": accountID, "pattern_locks_attributes": patternLocks};
          return userRepository.one('patterns').customPOST(pattern, null, undefined, {'Content-Type': 'application/json'});
        }

        function getPatternById(patternId) {
          return userRepository.one('patterns', patternId).get();
        }

        function savePatternName(name, patternId) {
          var updObject = {"pattern": {
                              "name" : name
                              } };
          return userRepository.one('patterns', patternId).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})
        }

        function updateSensorLock(patternId, sensorId, lock) {
          var updObject = {"lock": lock};
          return userRepository.one('patterns', patternId).one('pattern_lock', sensorId).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})
        }

      function deletePattern(id){
        return userRepository.one('patterns', id).remove();
      }


      return gatewayService;
    }
})();
