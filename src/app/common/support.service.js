(function() {
    'use strict';

    angular
        .module('secual')
        .service('SupportService', SupportService);

  SupportService.$inject = ['Restangular'];
    function SupportService(Restangular) {
        var supportService = {
          getFaqs: getFaqs,
          faqCategories: faqCategories,
          getFaqByCatId: getFaqByCatId,
          contactProperties: contactProperties,
          supportTitles: supportTitles,
          sendmail: sendmail
        };


        var supportRepository = Restangular.one('user');

        function getFaqs() {
          return supportRepository.one('faqs').get();
        }

        function faqCategories() {
          return supportRepository.one('faqs').one('categories').get();
        }

        function getFaqByCatId(category_id) {
          return supportRepository.customGET("faqs", {category_id: category_id}, {'Content-Type': 'application/json'});
        }

        function contactProperties() {
          return supportRepository.one('properties').get();
        }

        function supportTitles() {
          return supportRepository.one('contact').one('support_titles').get();
        }

        function sendmail(formdata) {
          var updObject = {
            "contact" : formdata
          };

          return supportRepository.one('contact').customPOST(updObject, 'send_email', {}, {'Content-Type': 'application/json'});

        }

        //function create(faq) {
        //  var updObject = {"faq": {
        //      "question" : faq.question
        //    }
        //  };
        //
        //  return faqRepository.customPOST(updObject, '', {}, {'Content-Type': 'application/json'});
        //}

        return supportService;
    }
})();
