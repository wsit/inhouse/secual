(function() {
    'use strict';

    angular
        .module('secual')
        .service('SecurityService', SecurityService);

  SecurityService.$inject = ['Restangular'];
    function SecurityService(Restangular) {
        var securityService = {
            get: get
        };



        var ownerRepository = Restangular.one('user');

        function get() {
            return ownerRepository.one('securities').get();
        }

        return securityService;
    }
})();
