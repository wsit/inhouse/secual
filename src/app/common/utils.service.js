(function() {
  'use strict';

  angular
    .module('secual')
    .factory('utils', UtilsService);

  /** @ngInject */
  function UtilsService($timeout, toastr, SHOP_URL, $state) {
    return {
      debounce: function(fn, wait) {
        var timeoutPromise;
        return function() {
          var args = arguments;
          if (timeoutPromise) {
            $timeout.cancel(timeoutPromise);
          }
          timeoutPromise = $timeout(function() {
            fn.apply(null, args);
          }, wait);
          timeoutPromise.then(function() {
            timeoutPromise = null;
          });
        }
      },
      showMessage: function(messageType, message){
        switch(messageType){
           case 'warning':  toastr.warning(message); break;
           case 'info':  toastr.info(message); break;
           case 'error':  toastr.error(message); break;
           case 'success': toastr.success(message); break;
        } 
      },
      goToShop: function(pageName){
        window.location.href= SHOP_URL + pageName;
      },
      goToSupport: function(pageName){
        window.location.href= SUPPORT_URL;
      },
      goTo: function(state, params){
        $state.go(state, params);
      }
    };
  }
})();
