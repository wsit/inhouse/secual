(function() {
    'use strict';

    angular
        .module('secual')
        .service('GroupService', GroupService);

    GroupService.$inject = ['Restangular'];
    function GroupService(Restangular) {
        var groupService = {
            getById: getById,
            get: get,
            create: create,
            save: save,
            delete: remove,
            addUser: addUser,
            removeUser: removeUser,
            updateGroup: updateGroup
        };


        var ownerRepository = Restangular.one('owner');
        var ownerUserRepository = Restangular.one('user');

        function get() {
            return ownerUserRepository.one('groups').get();
        }

        function getById(groupId) {
           return ownerRepository.one('groups', groupId).get();
        }

        function create(group) {
          group['user_ids'] = group.users.map(function(user) {
            return user.id;
          });

          var updObject = {group: group};
          return ownerRepository.one('groups').customPOST(updObject, '', {}, {'Content-Type': 'application/json'});
        }

        function save(group) {
            var updObject = {group: group};
            return ownerRepository.one('groups', group.id).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})
        }

      function updateGroup(group) {
        var updObject = {group: group};

        return ownerRepository.one('groups', group.id).customPUT(updObject, null, undefined, {'Content-Type': 'application/json'})
      }

        function remove(groupId){

          return ownerRepository.one('groups', groupId).remove();

          //var deferred = $q.defer();
          //deferred.resolve({result: 'ok'});
          //return deferred.promise;
        }

        function addUser(groupId, userId) {
          var updObject = {userId: userId};
          return ownerRepository.one('groups', groupId).customPOST(updObject, 'adduser', {}, {'Content-Type': 'application/json'});
        }

        function removeUser(groupId, userId) {
          var updObject = {userId : userId};
          return ownerRepository.one('groups', groupId).customDELETE('removeuser', updObject, {'Content-Type': 'application/json'});
        }

        return groupService;
    }
})();
