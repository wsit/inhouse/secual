(function() {
  'use strict';

  angular
    .module('secual')
    .controller('email_confirmController', email_confirmController);


  function email_confirmController($location, toastr, $state, $scope, $auth, $stateParams, $filter) {
    var searchObject = $location.search();
    var account_confirmation_success = searchObject.redirect;
    var client_id = searchObject.client_id;
    var token = searchObject.token;
    var uid = searchObject.uid;
    $scope.INVALID_EMAIL = $filter('translate')("INVALID_EMAIL");

    $scope.account_confirmation_success = searchObject.account_confirmation_success;
    if(searchObject.account_confirmation_success === 'true'){
        $state.go('login.main');
    }else{
      $scope.account_confirmation_success=false;
    }
  }
})();
