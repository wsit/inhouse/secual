(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  function routerConfig($stateProvider) {
    $stateProvider
      .state('email_confirm', {
        url: '/email_confirm',
        templateUrl: 'app/email_confirm/email_confirm.html',
        controller: 'email_confirmController',
        controllerAs: 'email_confirm'
      });

  }
})();
