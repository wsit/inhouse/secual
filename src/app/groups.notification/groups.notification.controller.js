(function() {
  'use strict';

  angular
    .module('secual')
    .controller('GroupNotificationController', GroupNotificationController);

  /** @ngInject */
  function GroupNotificationController(toastr, group, GroupService, $scope, $state, $filter) {
    var vm = this;
    $scope.$parent.base.backButton = true;
    //$scope.$parent.base.closeButton = true;

    vm.group = group.group;
    vm.updateGroup = updateGroup;
    vm.administrator = $filter('translate')("administrator");
    vm.push = $filter('translate')("push");
    vm.explanation_about_push_notification = $filter('translate')("explanation_about_push_notification");
    vm.email = $filter('translate')("email");
    vm.explanation_about_email_notification = $filter('translate')("explanation_about_email_notification");
    vm.SMS = $filter('translate')("SMS");
    vm.explanation_about_sms_notification = $filter('translate')("explanation_about_sms_notification");
    vm.ETC = $filter('translate')("ETC");
    vm.lockable = $filter('translate')("lockable");
    vm.explanation_about_lockable_notification = $filter('translate')("explanation_about_lockable_notification");
    vm.administratable = $filter('translate')("administratable");
    vm.explanation_about_administratable = $filter('translate')("explanation_about_administratable");
    vm.alert_stoppable = $filter('translate')("alert_stoppable");
    vm.explanation_about_alert_stoppable = $filter('translate')("explanation_about_alert_stoppable");


    // Update user group
    function updateGroup() {
      GroupService.updateGroup(vm.group).then(function(result){
        toastr.success($filter('translate')("UPDATED"));
      }, function(response){
        toastr.error(response.message);
      });
    }

  }
})();
