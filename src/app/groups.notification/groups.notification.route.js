(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.groups.notification', {
        url: '/notification/:id',
        templateUrl: 'app/groups.notification/groups.notification.html',
        controller: 'GroupNotificationController',
        controllerAs: 'groupNotification',
        resolve: {
          group: function(GroupService, $stateParams){
            return GroupService.getById($stateParams.id);
          }
        }
      });

  }

})();
