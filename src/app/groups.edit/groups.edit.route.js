(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.groups.edit', {
        url: '/edit/:id',
        templateUrl: 'app/groups.edit/groups.edit.html',
        controller: 'GroupEditController',
        controllerAs: 'groupEditInfo',
        resolve: {
          group: function(GroupService, $stateParams){
            return GroupService.getById($stateParams.id);
          }
        }
      })
      .state('app.groups.new', {
        url: '/new',
        templateUrl: 'app/groups.edit/groups.edit.html',
        controller: 'GroupEditController',
        controllerAs: 'groupEditInfo',
        resolve: {
          group: angular.noop
        }
      });
  }
})();
