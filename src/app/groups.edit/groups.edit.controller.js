(function () {
  'use strict';

  angular
    .module('secual')
    .controller('GroupEditController', GroupEditController);

  /** @ngInject */
  function GroupEditController(toastr, group, GroupService, $rootScope, $scope, $state, ModalService, $filter) {
    var baseCtrl = $scope.$parent.base;
    group = group ? group.plain().group : {
      users: [baseCtrl.user.owner],
      otherusers: [].concat(baseCtrl.user.users || []).filter(function (user) {
        return user.id !== baseCtrl.user.owner.id;
      })
    };

    var vm = this;
    vm.group = group;
    vm.newGroup = vm.editGroupName = angular.isUndefined(group.id);
    vm.save = saveGroup;
    vm.delete = deleteGroup;
    vm.addUser = addGroupUser;
    vm.removeUser = removeGroupUser;
    vm.edit = editGroup;
    vm.cancel = canceleditGroup;
    vm.group_name = $filter('translate')("group_name");
    vm.member = $filter('translate')("member");
    vm.administrator = $filter('translate')("administrator");
    vm.remove_group = $filter('translate')("remove_group");
    vm.other_user = $filter('translate')("other_user");
    vm.explanation_about_groups = $filter('translate')("explanation_about_groups");
    vm.all_members_have_been_added_to_this_group = $filter('translate')("all_members_have_been_added_to_this_group");


    baseCtrl.title = (vm.group.id ? $filter('translate')("edit_group") : $filter('translate')("new_group"));
    baseCtrl.setButtons({
      cancelButton: function () {
        // $state.go('app.groups.details', {'id':  vm.group.id});
        if($rootScope.flag_check)
          $state.go('app.groups.details', {'id':  vm.group.id});
        else
          $state.go('app.groups.list');;

      },
      checkButton: function () {
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
        var promise = vm.newGroup ? createGroup() : saveGroup();
        promise.then(function () {
          $state.go('app.groups.details', {'id': vm.group.id});
        });
      }
    });

    function AlertDeleteGroup() {
      ModalService.showModal({
        templateUrl: "app/components/alertModal/alertModal.html",
        controller: "ModalController"
      }).then(function (modal) {
        //it's a bootstrap element, use 'modal' to show it
        modal.element.modal();
        modal.close.then(function (result) {
          $(".modal-backdrop").css("display", "none");
          if (result == 'Yes') {
            GroupService.delete(vm.group.id).then(function (result) {
              $state.go('app.groups.list');
              toastr.success($filter('translate')("REMOVED"));
            }, function (response) {
              responseError(response);
            });
          }
        });
      })
    }

    function alertRemoveGroupUser(user, index) {
      ModalService.showModal({
        templateUrl: "app/components/alertModal/alertModal.html",
        controller: "ModalController"
      }).then(function (modal) {
        //it's a bootstrap element, use 'modal' to show it
        modal.element.modal();
        modal.close.then(function (result) {
          $(".modal-backdrop").css("display", "none");

          if (result == 'Yes') {
            GroupService.removeUser(vm.group.id, user.id).then(function (result) {
              toastr.success($filter('translate')("user_removed"));
            }, function (response) {
              responseError(response);
              vm.group.otherusers.pop();
              vm.group.users.splice(index, 0, user);
            });
          }
        });
      })
    }


    // Create new user group
    function createGroup() {
      if (angular.isUndefined(vm.group.name)) {
        toastr.error($filter('translate')("group_name_cannot_be_empty"));
      } else {
        return GroupService.create(vm.group).then(function (result) {
          vm.group.id = result.id;
          toastr.success($filter('translate')("created_new_group"));
        }, function (response) {
          responseError(response);
        });
      }
    }

    // Save user group
    function saveGroup() {
      return GroupService.updateGroup(vm.group).then(function (result) {
        toastr.success($filter('translate')("SAVED"));
      }, function (response) {
        responseError(response);
      });
    }

    // Delete user group
    function deleteGroup() {
      AlertDeleteGroup();
    }

    // Add new user to group
    function addGroupUser(user, index) {
      vm.group.otherusers.splice(index, 1);
      vm.group.users.push(user);

      if (vm.newGroup) {
        return;
      }

      GroupService.addUser(vm.group.id, user.id).then(function (result) {
        toastr.success($filter('translate')("user_added"));
      }, function (response) {
        responseError(response);
        vm.group.users.pop();
        vm.group.otherusers.splice(index, 0, user);

      });
    }

    // Remove user from group
    function removeGroupUser(user, index) {
      vm.group.users.splice(index, 1);
      vm.group.otherusers.push(user);

      if (vm.newGroup) {
        return;
      }
      alertRemoveGroupUser(user, index);

    }

    // Failed response handler
    function responseError(response) {
      if (response.status < 0) {
        toastr.error($filter('translate')("network_error"));
      } else {
        toastr.error(response.statusText);
      }
    }

    // Store group name to $scope
    function editGroup() {
      $scope.group_name = vm.group.name;
      console.log($scope.group_name);

    }

    // Store group name to $scope
    function canceleditGroup() {
      vm.group.name = $scope.group_name
    }

  }

})();
