(function() {
  'use strict';

  angular
    .module('secual')
    .controller('UsersController', UsersController);

  /** @ngInject */
  function UsersController(toastr, users, $scope, $state, accounts, utils, $filter) {
    var vm = this;
    vm.userAccount = accounts.account;

    vm.users = users;
    vm.goTo = goTo;
    vm.administrator = $filter('translate')("administrator");
    vm.explanation_text_about_users = $filter('translate')("explanation_text_about_users");


    $scope.$parent.base.title =　$filter('translate')("USERS");
    $scope.$parent.base.setButtons({
      menuButton: true,
      addButton: function() {
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
        if (isAllowedToAdd())
            $state.go('app.users.add');
         else utils.showMessage('error', "You are not allowed to add more users for this account");
      }
    });

    function isAllowedToAdd(){
      return vm.userAccount.max_users > vm.users.length;
    }


    function goTo(newState, params){
      $state.go('app.'+newState, params);
    }

  }
})();
