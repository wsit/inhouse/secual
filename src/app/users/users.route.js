(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.users', {
        url: '/users',
        abstract: true,
        template: '<ui-view />'
      })

      .state('app.users.list', {
        url: '',
        templateUrl: 'app/users/users.html',
        controller: 'UsersController',
        controllerAs: 'usersList',
        resolve: {
          users: function(UserService){
            return UserService.getUsers();
          },
          accounts: function (UserService){
             return UserService.getUserAccounts();
          }
        },
         data:{
          roles: ['owner', 'user'],
          isUserManageble: true
        }
      });

  }

})();
