(function() {
    'use strict';

    angular
        .module('secual')
        .filter('notificationFilter', notificationfilter);

    function notificationfilter() {

        return function (items, filterValue) {
            var result = [];
            if (filterValue == 'alert') {
              if (!items) return [];
              result = items.filter(function(item){
                return item.eventType == 1
              });
            } else if (filterValue == 'all') {
              //result = items.filter(function(item){
              //  return item.alert = false;
              //});
              if (!items) return [];
              result = items;
            } 
            else if (filterValue == 'regular') {
              if (!items) return [];
              result = items.filter(function(item){
                return item.eventType == 2 || item.eventType == 3
              });
            }

            return result;
        };
    }

})();
