(function() {
    'use strict';

    angular
        .module('secual')
        .service('NotificationService', NotificationService);

    NotificationService.$inject = ['Restangular', '$q'];
    function NotificationService(Restangular, $q) {
        var notificationService = {
                get: get,
                getById: getById,
                confirm : confirm,
                confirmAll: confirmAll
        };

        var notificationRepository = Restangular.one('user');

        function get() {
            return notificationRepository.one('secual_events').get();
            //var deferred = $q.defer();

            //var result = [{notificationType: 'comment', date:'2015.01.01 00:00 AM', description:'これで１０文字です。'},
            //{notificationType: 'user', date:'2015.01.01 00:00 AM', description:'これで１０文字です。これで２０文字です。これで３０文字です。'},
            //{notificationType: 'alert--success', date:'2015.01.01 00:00 AM', description:'これで１０文字です。これで２０文字です。これで３０文字です。これで４０文字です。これで５０文字です。', link_text:'タップして詳細を確認'}]

            //deferred.resolve(result);
            //return deferred.promise;

        }

         function getById(eventId) {
            return notificationRepository.one('secual_events', eventId).get();
        }


        function confirm(eventId) {
            return notificationRepository.one('secual_events', eventId).one('confirm').remove();
        }

        function confirmAll() {
            return notificationRepository.one('secual_events').one('confirm').remove();
        }

        return notificationService;
    }
})();
