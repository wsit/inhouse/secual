(function() {
  'use strict';

  angular
    .module('secual')
    .controller('NotificationsController', NotificationsController);

  /** @ngInject */
  function NotificationsController($state, $scope, NotificationService, utils, $filter) {
    var vm = this;
    vm.loaded = false;

    NotificationService.get().then(function(response){
       var _nots = response.plain().secual_events;
        vm.notifications = _nots.filter(function(not){
          return not.confirmed == false
        });
        vm.loaded = true;
      console.log(vm.notifications);

    }, function(reason){
      utils.showMessage($filter('translate')("error"), $filter('translate')("error_during_loading_the_page_occured"))
    })



    vm.filter = filter;
    vm.filterValue = 'all';
    vm.clear = clear;
    vm.GoTo = goTo;
    vm.clear_all_of_the_contents_of_the_notification = $filter('translate')("clear_all_of_the_contents_of_the_notification");
    vm.no_new_notification = $filter('translate')("no_new_notification");



    $scope.$parent.base.title = $filter('translate')("all_notification");

    $scope.$parent.base.setButtons({
      closeButtonRight: function() {
          $state.go('app.home');
         },
       menuButton:true
    });

    function filter(type){
      vm.filterValue = type;
    }

    function clear(){
      NotificationService.confirmAll().then(function(response){
         utils.showMessage('success', $filter('translate')("all_messages_have_been_successfully_confirmed"));
         vm.notifications.length=0;
      }, function(reason){
         utils.showMessage('error', $filter('translate')("error_during_confirmation_events_occured"));
      })

    }

    function goTo(newState, params, eventType){
      if (eventType != 1)
        return;
      $state.go('app.'+newState, params);
    }

  }
})();
