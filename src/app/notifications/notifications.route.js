(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.notifications', {
        url: '/notifications',
        templateUrl: 'app/notifications/notifications.html',
        controller: 'NotificationsController',
        controllerAs: 'notification'
        /*resolve: {
          notifications: function(NotificationService){ 
            return NotificationService.get();
          }
        }*/
      });

  }

})();
