(function() {
  'use strict';

  //XXX: ngTouch will cause ui-sref to stop working on touch devices

  angular
    .module('secual', ['ngAnimate',
    	'ngCookies',
    	/*'ngTouch',*/ 'ngSanitize',
    	'ngMessages',
    	'ngAria',
    	'restangular',
    	'ui.router',
    	'ngMaterial',
        'ipCookie',
    	'toastr',
    	'ng-token-auth',
    	'pascalprecht.translate', // translate module
      'tmh.dynamicLocale',
      'angularModalService',
      'angular-loading-bar'
    ]
    ).config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
  }])

})();
