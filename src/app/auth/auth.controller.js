(function () {
  'use strict';

  angular
    .module('secual')
    .controller('NativeAuthController', NativeAuthController)

  /** @ngInject */
  function NativeAuthController ($state, $location, NativeAuthFactory) {
    // expecting
    // localhost:3000/#/native_auth?access_token=A1eI2gwR5utPLZo9AVTZPg&client=dDZMyhwGqu74oZatqnq_4g&uid=owner1@secual.com&redirect=/
    var searchObject = $location.search()
    var redirUrl = searchObject.redirect
    var accessToken = searchObject.access_token
    var client = searchObject.client
    var uid = searchObject.uid

    console.log('accessToken:', accessToken)
    console.log('client:', client)
    console.log('uid:', uid)

    NativeAuthFactory.authenticateByToken(accessToken, client, uid)
      .then(function () {
        $location.url(redirUrl)
      }, function () {
        $state.go('login.main')
      })
  }

})();
