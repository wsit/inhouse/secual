(function () {
  'use strict'

  angular
    .module('secual')
    .config(routerConfig)

  /** @ngInject */
  function routerConfig ($stateProvider) {
    $stateProvider
      .state('native_auth', {
        url: '/native_auth',
        controller: 'NativeAuthController'
      })
  }
  
})();
