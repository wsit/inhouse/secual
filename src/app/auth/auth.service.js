(function () {
  'use strict';

  angular
    .module('secual')
    .factory('NativeAuthFactory', NativeAuthFactory)

  NativeAuthFactory.$inject = ['$auth', '$q']
  function NativeAuthFactory ($auth, $q) {
    var nauth = {}

    nauth.authenticateByToken = function (accessToken, client, uid) {
      return $q.when($auth.rejectDfd('clear dfd for token auth'))
        .then(function () {
          $auth.setAuthHeaders({
            'access-token': accessToken,
            'client': client,
            'uid': uid,
            'expiry': Math.ceil(Date.now() / 1000 + 300),
            'token-type': 'Bearer'
          })
          return $auth.validateUser()
        })
    }

    return nauth
  }

})();
