(function() {
  'use strict';

  angular
    .module('secual')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController(toastr, $state, $auth, $filter, utils) {
    var vm = this;
    vm.user = {};
    //remove this on prod enviropment
    //vm.user.email = 'kazuhiro.aoyagi@secual-inc.com'; // temp
    //vm.user.password = '12345678';
    vm.user.email = ''; // temp
    vm.user.password = '';
    vm.errors = {};
    vm.SignIn = SignIn;
    vm.SignOut = SignOut;
    vm.emailPlaceholder = $filter('translate')("EMAIL");
    vm.passwdPlaceholder = $filter('translate')("PASSWORD");
    vm.about = $filter('translate')("ABOUT");
    vm.forgetPassWd = $filter('translate')("FORGET_PASSWD");
    vm.signInLabel = $filter('translate')("SIGN_IN");
    vm.validators = {
      password: validatePassword,
      email: validateEmail
    }

    function SignIn(){
      if(validateForm()) {
        $auth.submitLogin(vm.user)
          .then(function (resp) {
            $state.go('app.home');
            // handle success response
          })
          .catch(function (resp) {
            console.log(resp);
            if (resp === undefined){
              utils.showMessage('error', $filter('translate')("something_went_wrong"));
            }
            else if(resp.hasOwnProperty('errors')){
              if (resp.reason === 'unauthorized'){
                utils.showMessage('error', $filter('translate')("username_password_incorrect"));
              }
            }
            // handle error response
            console.log(resp);
          });
      }
      else return;
    }

    function SignOut () {
      $auth.signOut()
        .then(function(resp) {
          $state.go('login.main');
          // handle success response
        })
        .catch(function(resp) {
          // handle error response
          console.log(resp);
        });
    }

    function validateForm() {
      return Object.keys(vm.validators).map(function(name) {
        return vm.validators[name]();
      }).every(function(valid) {
        return valid;
      });
    }

    function validateEmail() {
      vm.errors.email = '';
      vm.user.email = (vm.user.email || '').trim();
      if (!vm.user.email) {
        var m = $filter('translate')("EMAIL_IS_EMPTY");
        vm.errors.email = m;
        return false;
      }
      if(vm.user.email.length > 40){
        var m = $filter('translate')("EMAIL_TO_LONG");
        vm.errors.email = m;
        return false;
      }
      if (!/\S+@\S+\.\S+/.test(vm.user.email)) {
        var m = $filter('translate')("EMAIL_INVALID");
        vm.errors.email = m;
        return false;
      }

      return true;
    }

    function validatePassword() {
      vm.errors.password = '';
      if (vm.user.password.length > 20) {
        var m = $filter('translate')("PASSWORD_IS_TO_LONG");
        vm.errors.password = m;
        return false;
      }
      return true;
    }

  }
})();
