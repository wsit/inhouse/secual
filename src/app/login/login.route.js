(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('login', {
        url: '',
        abstract: true,
        template: '<ui-view />'
      })

      .state('login.main', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })
       .state('login.signout', {
         url: '/signout',
         templateUrl: 'app/login/login.html',
         controller: 'LoginController',
         controllerAs: 'login',
         resolve: {
           auth: function ($auth) {
             return $auth.signOut();
           }
         }
       });

  }

})();
