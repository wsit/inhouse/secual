(function () {
  'use strict';

  angular
    .module('secual')
    .run(runBlock)

  /** @ngInject */
  function runBlock (Restangular, $state, $rootScope, ipCookie) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (toState.data && toState.data.roles) {
        var user = ipCookie('secual-user');
         if (user){
           if (toState.data.roles.indexOf(user.role.name) == -1
            || !hasEnoughPermissions(user, toState.data.isGroupManageble, toState.data.isUserManageble, toState.data.isSecuritySettable)){
            event.preventDefault();
             $state.go('public.notfound');
           }
         }
      }
    })


    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      $rootScope.$broadcast('event:stateChangeFinished')
    })

    Restangular.setErrorInterceptor(function (response, deferred, responseHandler) {
      if (response.status === 401 && $state.current.name !== 'login') {
        $state.go('login.main');
        return false; // error
      }
      return true; // error not handled
    })

    Restangular.addRequestInterceptor(function(element) {
      // console.log("Request started");
      return element;
    });

    Restangular.addResponseInterceptor(function(data) {
      // console.log("Request returned");
      return data;
    });
  }
  function hasEnoughPermissions(user, isGroupManageble, isUserManageble, isSecuritySettable){
    if (isGroupManageble == undefined && isSecuritySettable == undefined && isUserManageble == undefined)
      return true;

    if(isGroupManageble != undefined && user.is_group_manageable == isGroupManageble){
      return true;
    }

    if(isSecuritySettable != undefined && user.is_security_settable == isSecuritySettable){
      return true;
    }
    if(isUserManageble != undefined && user.is_user_manageable == isUserManageble)
      return true;


    return false;
  }

})();
