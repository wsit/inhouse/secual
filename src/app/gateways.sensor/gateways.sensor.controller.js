(function() {
  'use strict';

  angular
    .module('secual')
    .controller('GatewaysSensorController', GatewaysSensorController);

  /** @ngInject */
  function GatewaysSensorController(toastr, sensor, UserGatewayService, $scope, $state, $stateParams, utils, $filter) {
    var vm = this;
    vm.sensor = sensor.plain();
    vm.gatewayid = $stateParams.gid;
    vm.save = utils.debounce(saveSensor, 300);
    vm.SIGNAL_STRONG = $filter('translate')("SIGNAL_STRONG");
    vm.SIGNAL_WEAK= $filter('translate')("SIGNAL_WEAK");
    vm.BLUETOOTH_DISABLED = $filter('translate')("BLUETOOTH_DISABLED");
    vm.Battery = $filter('translate')("Battery");
    vm.SENSOR = $filter('translate')("SENSOR");
    vm.move_the_sensor_to_another_gateway = $filter('translate')("move_the_sensor_to_another_gateway");
    vm.SENSITIVITY_SETTING = $filter('translate')("SENSITIVITY_SETTING");
    vm.SENSITIVITY = $filter('translate')("SENSITIVITY");
    vm.FREQUENCY = $filter('translate')("FREQUENCY");
    vm.explanation_text_sensor = $filter('translate')("explanation_text_sensor");
    vm.goTo = goTo;

    $scope.$parent.base.title = vm.sensor.name;
    $scope.$parent.base.setButtons({
      backButton: true
    });

    // Updating sensor data
    function saveSensor() {
      UserGatewayService.updateSensorData(vm.gatewayid, vm.sensor).then(function(result){
        $scope.$parent.base.title = vm.sensor.name;
        toastr.success($filter('translate')("保存しました"));
      }, function(response){
        if (response.status==-1 || response.status==0)
          toastr.error("センサー情報の設定に失敗しました");
        else
          toastr.error("センサー情報の設定に失敗しました");

      });
    }

    function goTo(newState, params){
      $state.go('app.'+newState, params);

    }

  }
})();
