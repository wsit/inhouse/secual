(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.gateways.sensor', {
        url: '/:gid/sensor/:sid',
        templateUrl: 'app/gateways.sensor/gateways.sensor.html',
        controller: 'GatewaysSensorController',
        controllerAs: 'gSensor',
        resolve: {
          sensor: function(UserGatewayService, $stateParams){
            return UserGatewayService.getSensorById($stateParams.gid, $stateParams.sid);
          }
        },
          data:{
            roles: ['owner', 'user'],
            isSecuritySettable: true
          }
      });

  }

})();
