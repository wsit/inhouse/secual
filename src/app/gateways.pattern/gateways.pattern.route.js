(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.gateways.pattern', {
        url: '/pattern/:id',
        templateUrl: 'app/gateways.pattern/gateways.pattern.html',
        controller: 'GatewaysPatternController',
        controllerAs: 'gPattern',
        resolve: {
          pattern: function(UserGatewayService, $stateParams){
            return UserGatewayService.getPatternById($stateParams.id);
          }
        }
      });

  }

})();
