(function() {
  'use strict';

  angular
    .module('secual')
    .controller('GroupsController', GroupsController);

  /** @ngInject */
  function GroupsController(toastr, groups, $rootScope, $scope, $state, ModalService, $filter) {
    var vm = this;
    vm.groups = groups.groups;
    vm.goTo = goTo;

    // $scope.$parent.base.title = 'Groups';
    $scope.$parent.base.title = $filter('translate')("title");
    $scope.$parent.base.setButtons({
      menuButton: true,
      addButton: function() {
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
        $rootScope.flag_check = false;
       $state.go('app.groups.new');
      }
    });

    function goTo(newState, params){
      $state.go('app.'+newState, params);
    }

  }
})();

angular.module('secual').directive('banner', function() {
  return function (scope, element, attrs) {
      element.height($(document).height());
  }
});
