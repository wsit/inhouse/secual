(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.groups', {
        url: '/groups',
        abstract: true,
        template: '<ui-view />'
      })

      .state('app.groups.list', {
        url: '',
        templateUrl: 'app/groups/groups.html',
        controller: 'GroupsController',
        controllerAs: 'groupsList',
        resolve: {
          groups: function(GroupService){
            return GroupService.get();
          }
        },
          data:{
            roles: ['owner', 'user'],
            isGroupManageble: true
          }
        
      });

  }

})();
