(function() {
  'use strict';

//angular
  //  .module('secual')
  //  .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.users.add_old', {
        url: '/new_old',
        templateUrl: 'app/users.add/users.add.html',
        controller: 'UserAddController',
        controllerAs: 'userAddInfo',
        resolve: {
          groups: function(GroupService){
            return GroupService.get();
          }
        }
      });

  }

})();
