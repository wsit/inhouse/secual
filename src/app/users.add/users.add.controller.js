(function() {
  'use strict';

  angular
    .module('secual')
    .controller('UserAddController', UserAddController);

  /** @ngInject */
  function UserAddController(toastr, groups, UserService, $scope, $state, $filter) {
    var vm = this;
    vm.user = {};
    vm.groups = groups.groups;
    vm.create = createUser;

    vm.name = $filter('translate')("name");
    vm.PASSWORD = $filter('translate')("PASSWORD");
    vm.confirm_password = $filter('translate')("confirm_password");
    vm.EMAIL = $filter('translate')("EMAIL");
    vm.phone = $filter('translate')("phone");
    vm.groups = $filter('translate')("groups");
    vm.save_user = $filter('translate')("save_user");

    $scope.$parent.base.title = $filter('translate')("add_user");
    $scope.$parent.base.setButtons({
      saveButton: createUser,
      cancelButton: true
    });

    function createUser(){
      vm.user.group_ids =[vm.group_id];

      UserService.create(vm.user).then(function(result){
        $state.go('app.users.list');
        toastr.success($filter('translate')("user_created"));
      }, function(response){
        var errors = response.data.error;
        var result = "";
        for (var i in errors) {
          if (errors.hasOwnProperty(i)) {
            result += errors[i];
            toastr.error(result);
          }
        }
      });

    }
  }
})();
