(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig)
    .controller('BaseController', BaseController);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('app', {
        url: '',
        abstract: true,
        templateUrl: 'app/common/application.html',
        controller: BaseController,
        controllerAs: 'base',
        resolve: {
          auth: function($auth, $q, ipCookie) {
            var deffered = $q.defer();
            $auth.validateUser().then(function(user){
              ipCookie('secual-user', user);
              deffered.resolve(user);
            },
            function(failed){
              ipCookie.remove('secual-user');
              deffered.reject(failed);
            });

            return deffered.promise;
          },
          user: function(UserService){
              return UserService.getDetails();
          }
        }

      });

    $urlRouterProvider.otherwise('/');
  }

  function BaseController(auth, user, $rootScope, $mdSidenav) {
    var vm = this;
    vm.user = {};
    vm.user = user.data;
    //vm.auth = auth;

    vm.setButtons = function(config) {
      config = config || {};
      vm.backButton = config.backButton || false;
      vm.closeButton = config.closeButton || false;
      vm.closeButtonRight = config.closeButtonRight || false;
      vm.editButton = config.editButton || false;
      vm.checkButton = config.checkButton || false;
      vm.saveButton = config.saveButton || false;
      vm.menuButton = config.menuButton || false;
      vm.addButton = config.addButton || false;
      vm.cancelButton = config.cancelButton || false;
      vm.notificationButton = config.notificationButton || false;
      vm.sendMailButton = config.sendMailButton || false;
      vm.connectButton = config.connectButton || false;
    };

    vm.lockStatusClass = '';

    vm.title = '';
    vm.setButtons();

    $rootScope.$on('event:stateChangeFinished', function(){
      $mdSidenav('right').close();

    });
    $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams){
      //$mdSidenav('right').close();
      //console.log('cange started')
    });
  }

})();
