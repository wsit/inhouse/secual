(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.account.payment', {
        url: '/payment',
        templateUrl: 'app/account.payment/account.payment.html',
        controller: 'AccountPaymentController',
        controllerAs: 'accountPaymentInfo',
        data:{
          roles: ['owner']
        },
        resolve: {
          account: function(UserService){
            return UserService.getUserAccounts();
          },
          paymentMethods: function(UserService){
            return UserService.getCreditCards();
          }
        }
      });
  }
})();
