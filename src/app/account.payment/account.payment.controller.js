(function() {
  'use strict';

  angular
    .module('secual')
    .controller('AccountPaymentController', AccountPaymentController);

  /** @ngInject */
  function AccountPaymentController($scope, $state, UserService, account, paymentMethods, utils, $filter) {
     var vm = this;
     vm.paymentMethods = paymentMethods.plain();

     vm.UNKNOWNTEXT = 'The text here should be copied from image';
     vm.owned_device = $filter('translate')("owned_device");
     vm.gateway = $filter('translate')("gateway");
     vm.SENSOR = $filter('translate')("SENSOR");
     vm.to_purchase_an_additional = $filter('translate')("to_purchase_an_additional");
     vm.payment = $filter('translate')("payment");
     vm.credit_card = $filter('translate')("credit_card");
     vm.UNSUBSCRIBE = $filter('translate')("UNSUBSCRIBE"); // not sure what text should be here 


    $scope.unsubscribe = unsubscribe;

    $scope.$parent.base.title = $filter('translate')("REGIST_INFO");//'登録情報';
    $scope.$parent.base.setButtons({
      cancelButton: function() {
        $state.go('app.account.show');
      },
      checkButton: function() {
        //save
        $state.go('app.account.show');
      }
    });
  }
})();
