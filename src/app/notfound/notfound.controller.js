(function() {
    'use strict';

    angular
        .module('secual')
        .controller('notFoundController', notFoundController);

    /** @ngInject */
    function notFoundController($scope, $filter) {
      
        $scope.you_done_have_access_to_requested_page_or_it_does_not_exist = $filter('translate')("you_done_have_access_to_requested_page_or_it_does_not_exist");

    }
})();
