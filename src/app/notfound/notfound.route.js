(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig)

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('public.notfound', {
        url: '/notfound',
         templateUrl: 'app/notfound/notfound.html'
         ,controller: 'notFoundController'
      });
  }



})();
