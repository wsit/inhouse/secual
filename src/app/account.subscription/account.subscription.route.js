(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.account.subscription', {
        url: '/subscription',
        templateUrl: 'app/account.subscription/account.subscription.html',
        controller: 'AccountSubscriptionController',
        controllerAs: 'accountSubscriptionPaymentInfo',
        data:{
          roles: ['owner']
        },
        resolve: {
          account: function(UserService){
            return UserService.getUserAccounts();
          },
          subscriptions: function(UserService){
            return UserService.getSubscriptionPlans();
          }
        }
      });
  }
})();
