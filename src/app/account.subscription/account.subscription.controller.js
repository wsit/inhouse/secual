(function() {
  'use strict';

  angular
    .module('secual')
    .controller('AccountSubscriptionController', AccountSubscriptionController);

  /** @ngInject */
  function AccountSubscriptionController($scope, subscriptions, $state, $filter) {
     var vm = this;
     vm.paymentMethods = paymentMethods.plain();

     vm.UNKNOWNTEXT = 'The text here should be copied from image like in row below'; 
     vm.UNSUBSCRIBE = $filter('translate')("UNSUBSCRIBE"); // not sure what text should be here 

    $scope.$parent.base.title = $filter('translate')("REGIST_INFO");//'登録情報'; //shoud be replaced
    

    $scope.$parent.base.setButtons({
      cancelButton: function() {
        $state.go('app.account.show');
      },
      checkButton: function() {
        //save
        $state.go('app.account.show');
      }
    });
  }
})();
