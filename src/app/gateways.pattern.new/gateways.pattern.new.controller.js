(function() {
  'use strict';

  angular
    .module('secual')
    .controller('GatewaysPatternAddController', GatewaysPatternAddController);

  /** @ngInject */
  function GatewaysPatternAddController(toastr, pattern, UserGatewayService, $scope, $state, ModalService, UserService, $filter) {
    var vm = this;
    console.log(pattern);
    vm.pattern = pattern.pattern;
    //vm.patternLocks = patternLocks.plain();
    //console.log(pattern);
    //vm.newPattern = angular.isUndefined(pattern.pattern);

   // if (vm.newPattern){
      $scope.$parent.base.title = "New Pattern";

      //vm.pattern = {};
      //vm.pattern.name = $filter('translate')("");

      UserService.getUserAccounts().then(function(response){
        vm.currentPatternId = response.account.current_pattern_id;
        UserGatewayService.getPatternById(vm.currentPatternId).then(function(response){
          vm.pattern_locks = patternGatewayName(response.pattern_locks);
        }, function(reason){

        })


      }, function(reason){

      })
    //}
    //else
    //{
    //  $scope.$parent.base.title =  vm.pattern.name;
//
  //  }


    vm.savePattern = savePattern;
    vm.deletePattern = deletePattern;
    vm.checkAllLocked = checkAllLocked;
    vm.updateSensorLock = updateSensorLock;
    vm.toggleSelectAll = toggleSelectAll;
    vm.createPattern = createPattern;
    vm.pattern_name = $filter('translate')("pattern_name");
    vm.LOCK_PATTERN = $filter('translate')("LOCK_PATTERN");
    vm.delete_lock_pattern = $filter('translate')("delete_lock_pattern");



    $scope.$parent.base.setButtons({
      cancelButton: function(){
        $state.go('app.gateways.list');
      },
      checkButton: function() {
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
        createPattern();
      }
    });


    function alertDeletePattern() {
      ModalService.showModal({
        templateUrl: "app/components/alertModal/alertModal.html",
        controller: "ModalController"
      }).then(function(modal) {
        //it's a bootstrap element, use 'modal' to show it
        modal.element.modal();
        modal.close.then(function(result) {
          $(".modal-backdrop").css("display", "none");
          console.log("result", result);
          if (result == 'Yes') {
            UserGatewayService.deletePattern(vm.pattern.id).then(function(result) {
              toastr.success("Removed");
              $state.go('app.gateways.list');
            }, function(response) {
              toastr.error(response.message);
            });
          }
        });
      })
    }

    // Get the name of gateway
    function patternGatewayName(pls) {
      var pattern_locks = [];
      // Separate gateways names and sensors
      angular.forEach(pls, function(value, key) {
        var pname = '';
        var sensors = [];
        var obj = {};

        pname = key;
        sensors = value;

        // get sensor status
        for (var i = 0; i < sensors.length; i++) {
          sensors[i].lock = sensors[i].lock === 0;
        }

        obj.name = pname;
        obj.sensors = sensors;

        pattern_locks.push(obj);
      });

      return pattern_locks;
    }

    // Check if all sensors are locked
    function checkAllLocked(inded, sensors) {
      var i_count = sensors.length;
      var l_count = 0;

      for (var i = 0; i < sensors.length; i++) {
        if (sensors[i].lock === true) {
          l_count++;
        }
      }

      return i_count === l_count;
    }

    // Updating sensor lock status
    function updateSensorLock(sensorId, lockStatus) {
      var lock = 1;
      if (lockStatus == true) lock = 0;

      UserGatewayService.updateSensorLock(vm.pattern.id, sensorId, lock).then(function(result) {
        //$state.go('app.gateways.list');
        console.log(result);
        toastr.success("Saved");
      }, function(response) {
        toastr.error(response.message);
      });
    }

    // Status toggle select
    function toggleSelectAll(val, sensors) {
      var lock = '1';
      var countHTTP = 1;
      var sensorCount = sensors.length;

      for (var i = 0; i < sensorCount; i++) {

        if (val) {
          lock = '0';
        }

        // Update sensor lock status
        UserGatewayService.updateSensorLock(vm.pattern.id, sensors[i].id, lock).then(function(result) {
          if (countHTTP === sensorCount) {
            // Update pattern lock status
            UserGatewayService.getPatternById(vm.pattern.id).then(function(result) {
              vm.pattern_locks = patternGatewayName(result.plain().pattern_locks);
            }, function(response) {
              vm.pattern_locks = patternGatewayName(pattern.plain().pattern_locks);
            });
            toastr.success($filter('translate')("UPDATED"));
          }

          countHTTP++;
        }, function(response) {
          toastr.error(response.message);
        });

      }
    }

    function createPattern(){
      if (vm.pattern.name == ''){
        toastr.error($filter('translate')("error_pattern_name_cant_be_empty"));
        return;
      }

      console.log(vm.pattern_locks);
      var allPatterns = [];
      for (var k=0; k < vm.pattern_locks.length; k++){
        for (var l = 0 ;l < vm.pattern_locks[k].sensors.length; l++){
          allPatterns.push({
            "sensor_id": vm.pattern_locks[k].sensors[l].id,
            "lock": vm.pattern_locks[k].sensors[l].lock
          });
        }
      }

      UserGatewayService.createPattern(vm.pattern.name, $scope.$parent.base.user.account_id, allPatterns).then(function(result) {
        $state.go('app.gateways.list');
        toastr.success($filter('translate')("Saved"));
      }, function(response) {
        toastr.error(response.message);
      });
    }

    // Update pattern name
    function savePattern() {
      UserGatewayService.savePatternName(vm.pattern.name, vm.pattern.id).then(function(result) {
        //$state.go('app.gateways.list');
        toastr.success($filter('translate')("Saved"));
      }, function(response) {
        toastr.error(response.message);
      });
    }

    function deletePattern() {
      alertDeletePattern();

    }
  }
})();
