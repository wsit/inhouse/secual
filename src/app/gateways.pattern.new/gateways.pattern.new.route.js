(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.gateways.new-pattern', {
        url: '/pattern/new',
        templateUrl: 'app/gateways.pattern.new/gateways.pattern.new.html',
        controller: 'GatewaysPatternAddController',
        controllerAs: 'gPatternAdd',
        resolve: {
          pattern: function(UserGatewayService, $stateParams){
            return UserGatewayService.getPatternById($stateParams.id);
          }
        }
      });

  }

})();

