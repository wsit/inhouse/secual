(function() {
  'use strict';

  angular
    .module('secual')
    .controller('AccountEditController', AccountEditController);

  /** @ngInject */
  function AccountEditController(toastr, $scope, $state, UserService, utils, $filter) {
    var vm = this;
    $scope.account = $scope.$parent.base.user;
    $scope.currentPassword = '';
    $scope.newPassword = '';
    $scope.confirmPassword = '';
    $scope.updateSubscription = updateSubscription;
    $scope.updatePassword = updatePassword;
    $scope.selectCreditCard = selectCreditCard;

    vm.goTo = utils.goTo;

    vm.validators = {
      name: validateName,
      email: validateEmail
    };

    vm.edit_photos= $filter('translate')("edit_photos");
    vm.contact_information = $filter('translate')("contact_information");
    vm.name = $filter('translate')("name");
    vm.kananame = $filter('translate')("kananame");
    vm.phone = $filter('translate')("phone");
    vm.email = $filter('translate')("e-mail");
    vm.address = $filter('translate')("street_address");
    vm.city = $filter('translate')("city");
    vm.zip = $filter('translate')("zip");
    vm.prefecture = $filter('translate')("prefecture");
    vm.PASSWORD = $filter('translate')("password");
    vm.new_password = $filter('translate')("new_password");
    vm.for_confirmation = $filter('translate')("for_confirmation");
    vm.payment = $filter('translate')("payment");
    vm.method = $filter('translate')("method");
    vm.credit_card = $filter('translate')("credit_card");
    vm.plan = $filter('translate')("plan");
    vm.basic = $filter('translate')("basic");
    vm.twentymonth= $filter('translate')("20month");
    vm.errors = {};


    // $scope.$parent.base.title = 'Edit Account';
    $scope.$parent.base.title = $filter('translate')("edit_account");
    $scope.$parent.base.setButtons({
      cancelButton: function(){
        $state.go('app.account.show');
      },
      checkButton: function() {
        //TODO: persist data
        //$state.go('app.account.show');
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
        updateUser();
      }
    });

    function validateForm() {
      return Object.keys(vm.validators).map(function(name) {

        return vm.validators[name]();
      }).every(function(valid) {
        return valid;
      });
    }

    function updateUser(){
      /*
       if ($scope.newPassword != '' && $scope.confirmPassword != ''){
       if ($scope.newPassword != $scope.confirmPassword)
       {
       utils.showMessage($filter('translate')("error"), $filter('translate')("PASSWORD_AND_CONFIRMATION_PASSWORD_SHOULD_BE_EQUAL"));//;'Password and Confirmation Password should be equal');
       return;
       }

       if ($scope.newPassword.length < 8){
       utils.showMessage($filter('translate')("error"), $filter('translate')("PASSWORD_SHOULD_CONTAIN_AS_LEAST_8_CHARACTERS"));//'Password should contains at least 8 characters');
       return;
       }

       updatePassword().then(function(){

       },function (failed) {
       utils.showMessage($filter('translate')("error"), $filter('translate')("ERROR_OCCURED_DURING_SAVING_USER_SETTINGS"));
       })

       }
       */


      vm.user = {};
      vm.user.name = $scope.account.name;
      vm.user.kananame = $scope.account.kananame;
      vm.user.phone = $scope.account.phone;
      vm.user.email = $scope.account.email;
      vm.user.address = $scope.account.address;
      vm.user.city =  $scope.account.city;
      vm.user.zip =  $scope.account.zip;
      vm.user.prefecture =  $scope.account.prefecture;
      vm.user.authenticationMethod = 2;


      if (!validateForm()) {
        return;
      }


      UserService.update(vm.user).then(function(response){
        toastr.success($filter('translate')("ACCOUNT_SAVED"));
        $state.go('app.account.show');
      }, function(failed){
        utils.showMessage($filter('translate')("error"), $filter('translate')("ERROR_OCCURED_DURING_SAVING_USER_SETTINGS"));//'Error occured during saving user settings. Please try later.');
        //show toaster here
      })

    }

    function validateName() {
      vm.errors.name = '';
      if (!vm.user.name || !vm.user.name.trim()) {
        vm.errors.name = $filter('translate')("user_name_cannot_be_empty");
        return false;
      }
      return true;
    }

    function validateEmail() {
      vm.errors.email = '';
      vm.user.email = (vm.user.email || '').trim();
      if (!vm.user.email) {
        vm.errors.email = $filter('translate')("email_cannot_be_empty");
        return false;
      }
      if (!/\S+@\S+\.\S+/.test(vm.user.email)) {
        vm.errors.email = $filter('translate')("INVALID_EMAIL");
        return false;
      }
      return true;
    }

    function updatePassword(){
      if ($scope.newPassword != '' && $scope.confirmPassword != ''){
        if ($scope.newPassword != $scope.confirmPassword)
        {
          utils.showMessage($filter('translate')("error"), $filter('translate')("PASSWORD_AND_CONFIRMATION_PASSWORD_SHOULD_BE_EQUAL"));//'Password and Confirmation Password should be equal');
          return;
        }
        var passwords = {"password": $scope.newPassword, "password_confirmation": $scope.confirmPassword};

        UserService.updatePassword(passwords).then(function(response){
          $state.go('app.account.show');
        }, function (reason) {
          utils.showMessage($filter('translate')("error"), $filter('translate')("ERROR_OCCURED_DURING_SAVING_USER_SETTINGS"));//'Error occured during saving user settings. Please try again later.');
        })
      }
    }

    function selectCreditCard(){

    }

    function updateSubscription(){
      var planId = 1;
      UserService.updateSubscription(planId).then(function(response){
        $state.go('app.account.show');
      }, function (reason) {
        utils.showMessage($filter('translate')("error"), $filter('translate')("ERROR_OCCURED_DURING_SAVING_USER_SETTINGS"));//'Error occured during saving user settings. Please try again later.');
      })
    }

  }
})();
