(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.account.edit', {
        url: '/edit',
        templateUrl: 'app/account.edit/account.edit.html',
        controller: 'AccountEditController',
        controllerAs: 'AccountEditInfo'
      });
  }
})();
