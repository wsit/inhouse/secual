(function() {
  'use strict';

   angular
    .module('secual')
      .constant('APP_VERSION', '0.0.1')
      .constant('API_URL', 'http://api.getsecual.com/v1/')
      .constant('SHOP_URL', 'http://getsecual.com')
      .constant('SUPPORT_URL', 'http://secual-inc.com')
      .constant('LOCALES', {
					    'locales': {
					        'ja_JP': 'Japanese',
					        'en_US': 'English'
					    },
    					'preferredLocale': 'ja_JP'
				})

})();
