(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.gateways.sensor-move', {
        url: '/:gid/sensor/:sid/move',
        templateUrl: 'app/gateways.sensor.move/gateways.sensor.move.html',
        controller: 'SensorMoveController',
        controllerAs: 'sensorMove',
        resolve: {
          gateways: function(UserGatewayService){
            return UserGatewayService.get();
          },
          sensor: function(UserGatewayService, $stateParams){
            return UserGatewayService.getSensorById($stateParams.gid, $stateParams.sid);
          }
        },
      });
  }

})();
