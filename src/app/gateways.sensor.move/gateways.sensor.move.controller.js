(function() {
  'use strict';

  angular
    .module('secual')
    .controller('SensorMoveController', SensorMoveController);

  /** @ngInject */
  function SensorMoveController(toastr, gateways,sensor, UserGatewayService, $scope, $state, utils, UserService, $filter) {
    var vm = this;
    vm.user = $scope.$parent.base.user;
    vm.sensor = sensor;
    console.log(vm.sensor);
    vm.gateways = gateways.plain().gateways;
    vm.getData = getdata;
    vm.goToShop = utils.goToShop;
    vm.updateSensor = updateSensor;
    vm.goTo = utils.goTo;

    vm.gateway = $filter('translate')("GATEWAY");


    $scope.$parent.base.title = 'Move';
    $scope.$parent.base.setButtons({
      backButton: true,
      checkButton: updateSensor
    });

    function updateSensor(){
      UserGatewayService.updateSensorData(vm.sensor.gateway_id, vm.sensor).then(function (result) {
        toastr.success("Saved");
      }, function (response) {
        toastr.error(response.message);
      })
    }

    function getdata(id){
      UserGatewayService.getById(id).then(function(result){
        toastr.success("Saved");
      }, function(response){
        toastr.error(response.message);
      })
    }

  }
})();
