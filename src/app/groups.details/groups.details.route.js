(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.groups.details', {
        url: '/details/:id',
        templateUrl: 'app/groups.details/groups.details.html',
        controller: 'GroupDetailsController',
        controllerAs: 'groupDetailsInfo',
        resolve: {
          group: function(GroupService, $stateParams){
            return GroupService.getById($stateParams.id);
          }
        },
        data:{
            roles: ['owner', 'user'],
            isGroupManageble: true
        }
      });

  }

})();
