(function() {
  'use strict';

  angular
    .module('secual')
    .controller('GroupDetailsController', GroupDetailsController);

  /** @ngInject */
  function GroupDetailsController(toastr, group, $rootScope, GroupService, utils, $scope, $state, $filter) {
    var vm = this;
    vm.group = group.plain().group;
    vm.save = utils.debounce(saveGroup, 1000);
    vm.goTo = goTo;
    vm.addMember = addMember;
    vm.USERNAME = $filter('translate')("USERNAME");
    vm.member = $filter('translate')("member");
    vm.administrator = $filter('translate')("administrator");
    vm.add_members = $filter('translate')("add_members");
    vm.push = $filter('translate')("push");
    vm.explanation_notification_type = $filter('translate')("explanation_notification_type");
    vm.call = $filter('translate')("call");
    vm.EMAIL = $filter('translate')("EMAIL");
    vm.SMS = $filter('translate')("SMS");
    vm.lockable = $filter('translate')("lockable");
    vm.Administratable = $filter('translate')("Administratable");
    vm.alert_stoppable = $filter('translate')("alert_stoppable");
    vm.explanation_about_permission = $filter('translate')("explanation_about_permission");
    vm.user_manageble = $filter('translate')("user_manageble");
    vm.group_manageble = $filter('translate')("group_manageble");
    vm.security_settable = $filter('translate')("security_settable");


    $scope.$parent.base.title = vm.group.name;
    $scope.$parent.base.setButtons({
      backButton: function() {
          $state.go('app.groups.list');
      },
      editButton: function() {
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
          $state.go('app.groups.edit', {id: vm.group.id});
      }
    });

    // Save user group
    function saveGroup() {
      GroupService.updateGroup(vm.group).then(function(result){
        toastr.success($filter('translate')("SAVED"));
      }, function(response){
        toastr.error(response.message);
      });
    }

    function goTo(newState, params){
      $state.go('app.'+newState, params);
    }

    function addMember() {
      $rootScope.flag_check = true;
      $state.go('app.groups.edit', {id: vm.group.id});
    }

  }
})();
