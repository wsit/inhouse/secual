(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig)


  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('public', {
        url: '/public',
        abstract: true,
        templateUrl: 'app/public/application_public.html',
        //controller: PublicController,
        controllerAs: 'publicBase'
      });
  }

})();
