(function() {
  'use strict';

  angular
    .module('secual')
    .controller('AccountController', AccountController);

  /** @ngInject */
  function AccountController($scope, $state, UserService, account, utils, $filter) {

    $scope.user = $scope.$parent.base.user;
    $scope.account = account.account;
    $scope.goToShop = utils.goToShop;
     UserService.getSubscription().then(function(response){
        $scope.subscription = subscription;
     }, function(reason){
        var message = '';
        if (!reason)
          message = $filter('translate')("ERROR_DURING_SUBSCRIPTION");//'Error occured during getting subscription';
        else message = reason.data.error;
         // utils.showMessage('warning', message);
     });

     var vm = this;
     vm.owned_device = $filter('translate')("owned_device");
     vm.gateway = $filter('translate')("gateway");
     vm.SENSOR = $filter('translate')("SENSOR");
     vm.to_purchase_an_additional = $filter('translate')("to_purchase_an_additional");
     vm.payment = $filter('translate')("payment");
     vm.credit_card = $filter('translate')("credit_card");
     vm.UNSUBSCRIBE = $filter('translate')("UNSUBSCRIBE"); // not sure what text should be here


    $scope.unsubscribe = unsubscribe;

    $scope.$parent.base.title = $filter('translate')("REGIST_INFO");//'登録情報';
    $scope.$parent.base.setButtons({
      menuButton: true,
      editButton: function() {
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
        $state.go('app.account.edit');
      }
    });

    function unsubscribe(){
      UserService.cancelSubscription().then(function(response){
        utils.showMessage('success', $filter('translate')("YOU_SUCCESSFLY_UNSUBSCRIBED"));//"You successfully unsubscribed");
      }, function(reason){
        utils.showMessage('error', $filter('translate')("ERROR_OCCURED_DURING_UNSUBSCRIPTION_PLEASE_TRY_AGAIN_LATER"));//"Error occured during unsubscription. Please try again later")
      })
    }
  }
})();
