(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.account', {
        url: '/account',
        abstract: true,
        template: '<ui-view />'
      })
      .state('app.account.show', {
        url: '',
        templateUrl: 'app/account/account.html',
        controller: 'AccountController',
        controllerAs: 'accountInfo',
        data:{
          roles: ['owner']
        },
        resolve: {
          account: function(UserService){
            return UserService.getUserAccounts();
          }
        }
      });
  }
})();
