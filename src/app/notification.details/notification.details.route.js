(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('app.notificationDetails', {
        url: '/notif-details/:id',
        templateUrl: 'app/notification.details/notification.details.html',
        controller: 'NotificationDetailsController',
        controllerAs: 'notifDetails',
        resolve: {
          notification: function(NotificationService, $stateParams){ 
            return NotificationService.getById($stateParams.id);
          }
        }
      });

  }

})();
