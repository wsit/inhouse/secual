(function() {
  'use strict';

  angular
    .module('secual')
    .controller('NotificationDetailsController', NotificationDetailsController);

  /** @ngInject */
  function NotificationDetailsController($scope, $state, notification, NotificationService, utils, $filter) {
    var vm = this;
    vm.notification = notification.plain().secual_event;
    vm.disableAlarmClass = '';

    vm.gateway_siren = $filter('translate')("gateway_siren");
    vm.sensor_warning_light = $filter('translate')("sensor_warning_light");
    vm.neighborhood_security_alert = $filter('translate')("neighborhood_security_alert");
    vm.to_confirmed_to_stop_the_alarm = $filter('translate')("to_confirmed_to_stop_the_alarm");
    vm.confirmed = $filter('translate')("confirmed");
    vm.num_100 = $filter('translate')("num_100");
    vm.emergency_contact = $filter('translate')("emergency_contact");
    vm.email = $filter('translate')("e-mail");
    vm.neighborhood_warning = $filter('translate')("neighborhood_warning");
    vm.delete = $filter('translate')("delete");


    if (vm.notification.confirmed)
      vm.disableAlarmClass = "green"

    vm.disableAlarm = disableAlarm;
    vm.goTo = goTo;

    $scope.$parent.base.title = $filter('translate')("alert_detail");

    //$scope.$parent.base.backButton = true;
    //$scope.$parent.base.closeButton = true;

    $scope.$parent.base.setButtons({
      backButton: true,
      closeButtonRight: function(){
        $state.go('app.home');
      }
    });

    function disableAlarm(){
      vm.disableAlarmClass = "green"
      confirmEvent(vm.notification.id);
    }

    function confirmEvent(eventId){
      NotificationService.confirm(eventId).then(function(response){
        utils.showMessage($filter('translate')("success"), $filter('translate')("current_event_has_been_successfully_confirmed"));
      }, function(reason){
        utils.showMessage($filter('translate')("error"), $filter('translate')("error_during_confirmation_event_occured"));
      })
    }

    function goTo(newState, params){
      $state.go('app.'+newState, params);
    }

  }
})();
