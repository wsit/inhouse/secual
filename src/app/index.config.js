(function() {
  'use strict';

  angular
    .module('secual')
    .config(config)
    .config(authConfig)
    .config(translationConfig)

  /** @ngInject */
  function config($logProvider, toastrConfig, RestangularProvider, API_URL) {
    // Enable log
    $logProvider.debugEnabled(true);

    RestangularProvider.setBaseUrl(API_URL);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = false;
    toastrConfig.progressBar = true;
  }

   /** @ngInject */
  function authConfig($authProvider, API_URL) {

    $authProvider.configure({
      apiUrl:                  API_URL,
      tokenValidationPath:     'auth/validate_token',
      signOutUrl:              'auth/sign_out',
      emailRegistrationPath:   'auth',
      accountUpdatePath:       'user',
      accountDeletePath:       'user',
      confirmationSuccessUrl:  window.location.href,
      passwordResetPath:       'user/password',
      passwordUpdatePath:      'user/password',
      passwordResetSuccessUrl: window.location.href,
      emailSignInPath:         'auth/sign_in',
      storage:                 'cookies',
      forceValidateToken:      false,
      validateOnPageLoad:      true,
      proxyIf:                 function() { return false; },
      //proxyUrl:                '/proxy',
      //omniauthWindowType:      'sameWindow',
      //authProviderPaths: {
      //  github:   '/auth/github',
      //  facebook: '/auth/facebook',
      //  google:   '/auth/google'
      //},
      tokenFormat: {
        "access-token": "{{ token }}",
        "token-type":   "Bearer",
        "client":       "{{ clientId }}",
        "expiry":       "{{ expiry }}",
        "uid":          "{{ uid }}"
      },
      cookieOps: {
        path: "/", 
        expires: 9999,
        expirationUnit: 'days',
        secure: false,
        domain: 'getsecual.com'
      },
      parseExpiry: function(headers) {
        // convert from UTC ruby (seconds) to UTC js (milliseconds)
        return (parseInt(headers['expiry']) * 1000) || null;
      },
      handleLoginResponse: function(response) {
        return response.data;
      },
      handleAccountUpdateResponse: function(response) {
        return response.data;
      },
      handleTokenValidationResponse: function(response) {
        return response.data;
      }
    });

  }

   /** @ngInject */
    function translationConfig($translateProvider, tmhDynamicLocaleProvider) {
        
        $translateProvider.useStaticFilesLoader({
            prefix: '../resources/locale-',
            suffix: '.json'
          });

        //$translateProvider.useStaticFilesLoader({
        //    prefix: '../resources/locale-', // path to translations files
        //    suffix: '.json'// suffix, currently- extension of the translations
        //});

        $translateProvider.useMissingTranslationHandlerLog();
        $translateProvider.preferredLanguage('ja_JP');

        tmhDynamicLocaleProvider.localeLocationPattern('/bower_components/angular-i18n/angular-locale_{{locale}}.js');
    }

})();
