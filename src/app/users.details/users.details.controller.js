(function() {
  'use strict';

 angular
    .module('secual')
    .controller('UserDetailsController', UserDetailsController);

  /** @ngInject */
  function UserDetailsController(toastr, user, groups, UserService, $scope, $state, $filter) {
    var vm = this;
    vm.user = user;
    vm.user.name = user.name || $filter('translate')("USERNAME");
    vm.groups = groupsThatUserIncluded(groups.groups, vm.user.id);
    vm.goTo = goTo;
    vm.administrator = $filter('translate')("administrator");
    vm.users_groups = $filter('translate')("users_groups");
    $scope.$parent.base.title = vm.user.name;
    $scope.$parent.base.setButtons({
      backButton: true,
      editButton: function() {
        $('#content-container').removeClass("view_transition_backward");
        $('#content-container').addClass("view_transition_forward");
        $state.go('app.users.edit', {id: vm.user.id});
      }
    });

    // Find groups that user belongs
    function groupsThatUserIncluded(groups, userId) {
      var groupsList = [];

      // Loop through groups
      for (var i = 0; i < groups.length; i++) {
        // Loop throug users inside group
        for (var j = 0; j < groups[i].users.length; j++) {
          // check for user as member in this group
          if (groups[i].users[j].id === userId) {
            // User found push group data
            groupsList.push(groups[i]);
          }
        }
      }

      return groupsList;
    }

    function goTo(newState, params){
      $state.go('app.'+newState, params);
    }

  }
})();
