(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.users.details', {
        url: '/:id',
        templateUrl: 'app/users.details/users.details.html',
        controller: 'UserDetailsController',
        controllerAs: 'userDetails',
        resolve: {
          user: function(UserService, $stateParams){
            return UserService.getById($stateParams.id);
          },
          groups: function(GroupService){
            return GroupService.get();
          }
        },
        data:{
            roles: ['owner', 'user'],
            isUserManageble: true
        }
      });

  }

})();
