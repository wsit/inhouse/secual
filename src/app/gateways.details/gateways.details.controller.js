(function() {
    'use strict';

    angular
        .module('secual')
        .controller('GatewaysDetailsController', GatewaysDetailsController);

    /** @ngInject */
    function GatewaysDetailsController(ModalService, $stateParams, toastr, gateway, gatewaySensors, UserGatewayService, $scope, $state, SHOP_URL, $filter) {
        var vm = this;
        vm.gateway = gateway.plain().gateway;
        vm.sensors = gatewaySensors.plain();
        vm.save = saveGateway;
        vm.edit = editGateway;
        vm.cancel = cancelGateway;
        vm.shopUrl = SHOP_URL;
        vm.goTo = goTo;
        vm.goToShop = goToShop;

        vm.GW = $filter('translate')("gateway");
        vm.SENSOR = $filter('translate')("SENSOR");
        vm.Bluetooth = $filter('translate')("Bluetooth");
        vm.ADD_SENSORS = $filter('translate')("ADD_SENSORS");
        vm.explanation_text_about_gateways_and_sensors = $filter('translate')("explanation_text_about_gateways_and_sensors");
        $scope.$parent.base.title = vm.gateway.name;
        $scope.$parent.base.setButtons({
            backButton: function() {
                $state.go('app.gateways.list');
            }
        });

        checkUpdates();

        function checkUpdates() {
            UserGatewayService.getById($stateParams.id).then(function(result) {
                var gateway = result.plain().gateway;
               if (gateway.otaaf != gateway.newaf) {
                        confirmUpdate(gateway.id);
                    }
            }, function(response) {
                if (response.status==-1 || response.status==0)
                                toastr.error($filter('translate')("error_during_update_occures_please_try_again_later"));
                            else
                            toastr.error(response.message);
            })
        }

        function confirmUpdate(gatewayId) {
            ModalService.showModal({
                templateUrl: "app/components/confirmUpdateModal/confirmUpdateModal.html",
                controller: "ModalController"
            }).then(function(modal) {
                //it's a bootstrap element, use 'modal' to show it
                modal.element.modal();
                modal.close.then(function(result) {
                    $(".modal-backdrop").css("display", "none");
                    if (result == 'Update') {
                        UserGatewayService.confirmGatewayUpdate(gatewayId).then(function(result){
                          toastr.success($filter('translate')("UPDATE_WILL_BE_AVAILABLE_SOON"))
                        },function(response){
                            if (response.status==-1 || response.status==0)
                                toastr.error($filter('translate')("error_during_update_occures_please_try_again_later"));
                            else
                            toastr.error(response.message);
                        })
                    }
                });
            })
        }

        function saveGateway() {
            UserGatewayService.save(vm.gateway).then(function(result) {
                //$state.go('app.gateways.list');
                toastr.success($filter('translate')("SAVED"));
                $scope.$parent.base.title = vm.gateway.name;
            }, function(response) {
                toastr.error(response.message);
            });
        }

        // Edit Gateway name will be stored in $scope.gateway_name
        function editGateway() {
            $scope.gateway_name = vm.gateway.name;
        }

        // Cancel edit gateway name will replace data from $scope.gateway_name
        function cancelGateway() {
            vm.gateway.name = $scope.gateway_name;
        }

        function goTo(newState, params) {
            $state.go('app.' + newState, params);
        }

        function goToShop(pageName) {
            window.location.href = vm.shopUrl + pageName;
        }

    }
})();
