(function() {
  'use strict';

  angular
    .module('secual')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
     $stateProvider
      .state('app.gateways.details', {
        url: '/:id',
        templateUrl: 'app/gateways.details/gateways.details.html',
        controller: 'GatewaysDetailsController',
        controllerAs: 'gDetails',
        resolve: {
          gateway: function(UserGatewayService, $stateParams){
            return UserGatewayService.getById($stateParams.id);
          },
          gatewaySensors: function(UserGatewayService, $stateParams){
            return UserGatewayService.getSensors($stateParams.id);
          }
        }
      });

  }

})();
