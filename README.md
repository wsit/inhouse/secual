** japanese version goes! ** here: http://cl.ly/3k471Y1U3E1D


### Installation

1. clone repository to local environment
2. npm install
3. bower install

##### running app in development mode(liveroad)

```sh
$ gulp serve
```

##### build distributive  

```sh
$ gulp clean
$ gulp build
```

### General rules

##### General info

Specs for Api rest endpoints you can find here https://github.com/secual/ruby_models/blob/master/API Interface.md  


##### Branching
* The main branch is master. Before begin to code new page - new branch with the name of page should be created. Push your changes there.
* Some pages contains common components (eg menu, login area etc). They should be created in new branch with name 'directive_'+name. That components almost always will be located in /app/common/directiveName/ folder, temaplates and styles should be also there.
* Do NOT merge your branch with master. It will be done by product manager after your code will be verified.

##### Dependencies 
* All external javascript libraries and modules related to angular.js part should be installed with bower
* All modules related to gulp and requires nodejs to run should be installed with npm install

##### Application folder structure
* In most cases one folder is lead to one page of website. It should contain html, css(scss, less), directives(used only on this page), route file, services that consumes data for this page.
* Common(shared) components that can be used on many pages should be storred on /app/common folder. It contains subfolders for directives, services, layouts. Templates that are related to specific dericitive should lay in directives/directiveName folder 

##### Application file names
* The html file for specific page should have contain pageName+'.'+'page.'+'html'
* Controller for specific page should have the name pageName+'.'+controller.js
* Route for specific page should have the name pageName+'.'+route.js
* Service for specific page should have the name pageName+'.'+service.js
* Directive for specific page should have the name pageName+'.'+directive.js. If directive contains template it should be in the same folder with name pageName+'.'+directive.scss
* Styles for specific page should lay in this folder with name pageName+'.'+scss

##### Special development requirements
* To consume rest endpoints it is required to use restangular ( http://www.ng-newsletter.com/posts/restangular.html)
* All CRUD requests should be moved to services(factories or providers)
* All DOM manipulations should be done inside directives
* We use sass preprocessor for styles
* If data need to be loaded to the page from server before or on page load it should be done in RESOLVE method of ui-route service for  specific route. https://github.com/angular-ui/ui-router/wiki
* If for some reason you still need to load the data to the page from server in controller, please do parallel requests using $q.all method 
* We use toaster for notitications it should be configured in config block of main app. 
* All fields in forms that marked with * are required, email, password(strenth or minimal length if applicable), password comfirmation should be verified on client side onblur event. If response status that is different from 200 received from the server toaster error notification should be shown   




